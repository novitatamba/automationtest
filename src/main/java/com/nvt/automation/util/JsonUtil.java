package com.nvt.automation.util;

import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;

import java.util.Map;

/**
 * Created by Thanh Chuong on 22/5/17.
 */
public class JsonUtil {
    public static String getAttribute(String json ,String path, String attribute){
        JSONArray array = JsonPath.read(json,path);
        Map<String, String> map = (Map)array.get(0);
        return map.get(attribute);
    }

    public static String getAttribute(JSONArray array, String attribute){
        Map<String, Object> map = (Map)array.get(0);
        return map.get(attribute).toString();
    }
}
