/*
 * Liquid Pay
 */


package com.nvt.automation.web.exception;

/**
 *
 * @author Thanh Chuong
 */
public class RUException extends Exception{
    public RUException(String message){
        super(message);
    }
}
