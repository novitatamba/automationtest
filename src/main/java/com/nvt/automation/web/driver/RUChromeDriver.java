/*
 * Zyllem
 */
package com.nvt.automation.web.driver;

import com.nvt.automation.web.util.SerenityUtil;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author Thanh Chuong
 */
public class RUChromeDriver implements DriverSource{
    @Override
    public WebDriver newDriver() {
        System.setProperty("webdriver.chrome.driver", SerenityUtil.getEnv("webdriver.chrome.driver"));
        WebDriver driver = new ChromeDriver();
        return driver;
//        return new FirefoxDriver();
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }
}
