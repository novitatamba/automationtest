package com.nvt.automation.steps.mvp1;

import com.nvt.automation.constants.mvp1.MVP1Endpoint;
import com.nvt.automation.databuilder.mvp1.LoginDataBuilder;
import com.nvt.automation.databuilder.mvp1.SendOTPRequestDataBuilder;
import com.nvt.automation.databuilder.mvp1.ValidationDataBuilder;
import com.nvt.automation.databuilder.mvp1.RegisterDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.StepsUtil;
import com.nvt.automation.util.JsonUtil;
import com.nvt.automation.web.util.SerenityUtil;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.minidev.json.JSONArray;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.json.JSONObject;


import static io.restassured.path.json.JsonPath.from;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static com.jayway.jsonpath.JsonPath.read;
//import static com.jayway.restassured.path.json.JsonPath.*;

/**
 * Created by Thanh Chuong on 22/5/17.
 */
public class MVP1ConsumerSteps {


    private static final String VALIDATE_STATUS="validate_status";
    private static final String DESC="description";
    private static final String MESSAGE="message";

    private static final String NAME="name";

    private static String ACCESS_TOKEN;
    private static String REFRESH_TOKEN;
    private static String EXPIRES_TOKEN;
    private static String NEW_ACCESS_TOKEN;
    private static String NEW_REFRESH_TOKEN;


    RequestSpecification requestSpecification = new RequestSpecBuilder()
            .addHeader("Liquid-Api-Key",SerenityUtil.getEnv("api.key"))
            .addHeader("Content-Type","application/json; charset=ISO-8859-1")
            .setBaseUri(StepsUtil.getAPIURL())
            .build();

    RequestSpecification requestAccessTokenSpesification;
    RequestSpecification requestRemoveAccessToken;
    RequestSpecification requestSpecificationNewAccessToken;


    @Step
    public void enterValidEmailAndPromoCode(){
        String json = ValidationDataBuilder.buildValidEmailAndPromoCode();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();

        JSONArray emailField = com.jayway.jsonpath.JsonPath.read(response,"$.list[?(@.name =='email_address')]");
        assertThat(JsonUtil.getAttribute(emailField,VALIDATE_STATUS),equalTo("true"));
        assertThat(JsonUtil.getAttribute(emailField,DESC),equalTo("This email is available to register."));

        JSONArray promoCodeField = com.jayway.jsonpath.JsonPath.read(response,"$.list[?(@.name =='promo_code')]");
        assertThat(JsonUtil.getAttribute(promoCodeField,VALIDATE_STATUS),equalTo("true"));
        assertThat(JsonUtil.getAttribute(promoCodeField,DESC),equalTo("N/A"));

    }

    @Step
    public void enterEmailThatHasBeenRegistered(){
        String json = ValidationDataBuilder.buildEmailHasBeenRegistered();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();

        JSONArray emailField = com.jayway.jsonpath.JsonPath.read(response,"$.list[?(@.name =='email_address')]");
        assertThat(JsonUtil.getAttribute(emailField,VALIDATE_STATUS),equalTo("false"));
        assertThat(JsonUtil.getAttribute(emailField,DESC),equalTo("This email address is already registered with us. Try logging in or use a different email."));
    }

    @Step
    public void enterInvalidEmailFormat(){
        String json = ValidationDataBuilder.buildInvalidEmailFormat();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();

        JSONArray emailField = read(response,"$.list[?(@.name =='email_address')]");
        assertThat(JsonUtil.getAttribute(emailField,VALIDATE_STATUS),equalTo("false"));
        assertThat(JsonUtil.getAttribute(emailField,DESC),equalTo("Invalid email format"));
    }

    @Step
    public void enterEmptyEmail(){
        String json = ValidationDataBuilder.buildEmptyEmail();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();

        JSONArray emailField = read(response,"$.list[?(@.name =='email_address')]");
        assertThat(JsonUtil.getAttribute(emailField,VALIDATE_STATUS),equalTo("false"));
        assertThat(JsonUtil.getAttribute(emailField,DESC),equalTo("Invalid email format"));
    }

    @Step
    public void enterInvalidPasswordTest(){
        String json = ValidationDataBuilder.buildInvalidPassword();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();
        JSONArray passwordField = read(response,"$.list[?(@.name =='password')]");
        assertThat(JsonUtil.getAttribute(passwordField,VALIDATE_STATUS),equalTo("false"));
        assertThat(JsonUtil.getAttribute(passwordField,DESC),equalTo("Password should be alphanumeric and between 8 to 20 characters."));
    }

    @Step
    public void enterEmptyPasswordTest(){
        String json = ValidationDataBuilder.buildEmptyPassword();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();
        JSONArray passwordField = read(response,"$.list[?(@.name =='password')]");
        assertThat(JsonUtil.getAttribute(passwordField,VALIDATE_STATUS),equalTo("false"));
        assertThat(JsonUtil.getAttribute(passwordField,DESC),equalTo("Password should be alphanumeric and between 8 to 20 characters."));
    }

    @Step
    public void enterRegisteredMobileNumber(){
        String json = ValidationDataBuilder.buildRegisteredMobile();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();
        JSONArray passwordField = read(response,"$.list[?(@.name =='mobile_number')]");
        assertThat(JsonUtil.getAttribute(passwordField,VALIDATE_STATUS),equalTo("false"));
        assertThat(JsonUtil.getAttribute(passwordField,DESC),equalTo("You might have registered this mobile number in another Liquid account. Please enter a different mobile number."));

    }

    @Step
    public void enterValidMobileNumberTest(){
        String json = ValidationDataBuilder.buildValidMobileNumber();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();
        JSONArray mobileNumberField = read(response,"$.list[?(@.name == 'mobile_number')]");
        assertThat(JsonUtil.getAttribute(mobileNumberField,VALIDATE_STATUS),equalTo("true"));
        assertThat(JsonUtil.getAttribute(mobileNumberField,DESC),equalTo("This mobile number is available to register."));
    }

    @Step
    public void enterValidDialingCodeTest(){
        String json = ValidationDataBuilder.buildValidDialingCode();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();
        JSONArray dialingCodeField = read(response,"$.list[?(@.name =='dialing_code')]");
        assertThat(JsonUtil.getAttribute(dialingCodeField,VALIDATE_STATUS),equalTo("true"));
        assertThat(JsonUtil.getAttribute(dialingCodeField,DESC),equalTo("This dialing code is available."));
    }

    @Step
    public void enterInvalidDialingCodeTest(){
        String json = ValidationDataBuilder.buildInvalidDialingCode();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();
        JSONArray dialingCodeField = read(response,"$.list[?(@.name =='dialing_code')]");
        assertThat(JsonUtil.getAttribute(dialingCodeField,VALIDATE_STATUS),equalTo("false"));
        assertThat(JsonUtil.getAttribute(dialingCodeField,DESC),equalTo("Dialing code is invalid"));
    }

    @Step
    public void enterEmptyDialingCodeTest(){
        String json = ValidationDataBuilder.buildEmptyDialingCode();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.VALIDATION)
                .then()
                .log().all()
                .extract().asString();
        JSONArray dialingCodeField = read(response,"$.list[?(@.name == 'dialing_code')]");
        assertThat(JsonUtil.getAttribute(dialingCodeField,VALIDATE_STATUS),equalTo("false"));
        assertThat(JsonUtil.getAttribute(dialingCodeField,DESC),equalTo("Dialing code is invalid"));
    }


    @Step
    public String login(Consumer consumer){
        String json = LoginDataBuilder.buildLoginJson(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.LOGIN)
                .then()
                .log().all()
                .extract().asString();
        if(from(response).get("type").equals("auth")){
            ACCESS_TOKEN = from(response).get("access_token").toString();
            REFRESH_TOKEN = from(response).get("refresh_token").toString();
            EXPIRES_TOKEN = from(response).get("expires_in").toString();
            requestAccessTokenSpesification = new RequestSpecBuilder()
                    .addHeader("Liquid-Api-Key",SerenityUtil.getEnv("api.key"))
                    .addHeader("Content-Type","application/json; charset=ISO-8859-1")
                    .addHeader("Authorization","Bearer "+ACCESS_TOKEN)
                    .setBaseUri(StepsUtil.getAPIURL())
                    .build();
        }
        return response;
    }

    @Step

    public String failedLogin(Consumer consumer){
        String json = LoginDataBuilder.buildLoginJson(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.LOGIN)
                .then()
                .log().all()
                .extract().asString();
        if(from(response).get("type").equals("errors")){
            requestRemoveAccessToken = new RequestSpecBuilder()
                    .addHeader("Liquid-Api-Key",SerenityUtil.getEnv("api.key"))
                    .addHeader("Content-Type","application/json; charset=ISO-8859-1")
                    .setBaseUri(StepsUtil.getAPIURL())
                    .build();
        }
        return response;
    }

    @Step
    public void loginWithValidUserAndPassword(){
        String response = login(LoginDataBuilder.buildConsumerWithValidCredential());
        assertThat(from(response).get("type"),equalTo("auth"));
        assertNotNull(from(response).get("access_token"));
        assertNotNull(from(response).get("refresh_token"));
    }

    @Step
    public void loginWithInvalidEmailAddress(){
        String response = failedLogin(LoginDataBuilder.buildConsumerWithInvalidEmailAddress());
        assertThat(from(response).get("type"),equalTo("errors"));
    }

    @Step
    public void loginWithInvalidPassword(){
        String response = failedLogin(LoginDataBuilder.buildConsumerWithInvalidPassword());
        assertThat(from(response).get("type"),equalTo("errors"));
    }

    @Step
    public void loginWithUnregisteredEmailAddress(){
        String response = failedLogin(LoginDataBuilder.buildConsumerWithUnregisteredEmailAddress());
        assertThat(from(response).get("type"),equalTo("errors"));
    }

    @Step
    public void loginWithEmptyEmailAddress(){
        String response = failedLogin(LoginDataBuilder.buildConsumerWithEmptyEmailAddress());
        assertThat(from(response).get("type"),equalTo("errors"));
    }

    @Step
    public void loginWithEmptyPassword(){
        String response = failedLogin(LoginDataBuilder.buildConsumerWithEmptyPassword());
        assertThat(from(response).get("type"),equalTo("errors"));
    }

    @Step
    public void loginWithEmptyEmailAddressAndEmptyPassword(){
        String response = failedLogin(LoginDataBuilder.buildConsumerWithEmptyEmailAddressAndEmptyPassword());
        assertThat(from(response).get("type"),equalTo("errors"));
    }

    @Step
    public void loginWithValidCredentialsWithPendingStatus(){
        String response = login(LoginDataBuilder.buildConsumerWithValidCredentialPendingStatus());
        assertThat(from(response).get("type"),equalTo("auth"));
        assertNotNull(from(response).get("access_token"));
        assertNotNull(from(response).get("refresh_token"));
    }

    @Step
    public String refreshToGetNewAccessToken(){
      String json = buildDataRefresh();
      String response = SerenityRest.rest().specification(requestSpecification)
              .given()
              .log().all()
              .when()
              .body(json)
              .post(MVP1Endpoint.REFRESH)
              .then()
              .log().all()
              .extract().asString();
        if(from(response).get("type").equals("auth")) {
            NEW_ACCESS_TOKEN = from(response).get("access_token").toString();
            NEW_REFRESH_TOKEN = from(response).get("refresh_token").toString();
            EXPIRES_TOKEN = from(response).get("expires_in").toString();
            requestSpecificationNewAccessToken = new RequestSpecBuilder()
                    .addHeader("Liquid-Api-Key",SerenityUtil.getEnv("api.key"))
                    .addHeader("Content-Type","application/json; charset=ISO-8859-1")
                    .addHeader("Authorization","Bearer "+NEW_ACCESS_TOKEN)
                    .setBaseUri(StepsUtil.getAPIURL())
                    .build();
        }
      return response;

    }

    public static String buildDataRefresh(){
        JSONObject json = new JSONObject();
        json.put("refresh_token",REFRESH_TOKEN);
        return json.toString();
    }


    @Step
    public void getRefreshNewToken(){
        String response = refreshToGetNewAccessToken();
        assertThat(from(response).get("type"),equalTo("auth"));
        assertNotNull(from(response).get("access_token"));
        assertNotNull(from(response).get("refresh_token"));
    }

    @Step
    public String expiryRefreshToken(){
        String json = buildDataRefresh();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REFRESH)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("errors"));
        return response;
    }

    public String getExpiresToken(){return EXPIRES_TOKEN;}


    @Step
    public String requestOTP(Consumer consumer){
        String json = RegisterDataBuilder.buildRequestOTPJson(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REQUEST_OTP)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("otp"));
        return response;
    }

    @Step
    public String register(Consumer consumer){
        String json = RegisterDataBuilder.buildRegisterJson(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REGISTRATION)
                .then()
                .log().all()
                .extract().asString();
        if(from(response).get("type").equals("consumer")){
            ACCESS_TOKEN = from(response).get("access_token").toString();
            REFRESH_TOKEN = from(response).get("refresh_token").toString();
            requestAccessTokenSpesification = new RequestSpecBuilder()
                    .addHeader("Liquid-Api-Key",SerenityUtil.getEnv("api.key"))
                    .addHeader("Content-Type","application/json; charset=ISO-8859-1")
                    .addHeader("Authorization","Bearer "+ACCESS_TOKEN)
                    .setBaseUri(StepsUtil.getAPIURL())
                    .build();
        }
        return response;
    }


    @Step
    public void registerWithValidCredential(){
        String response = register(RegisterDataBuilder.buildRegisterDataWithValidCredential());
        assertThat(from(response).get("type"),equalTo("consumer"));
        assertNotNull(from(response).get("access_token"));
        assertNotNull(from(response).get("refresh_token"));
    }

    public String getAccessToken(){return ACCESS_TOKEN;}

    @Step
    public String sendOTPRequestToValidMobileNumber() {
        String json = SendOTPRequestDataBuilder.buildOTPRequestJsonWithValidNuber();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REQUEST_OTP)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"), equalTo("otp"));
        return response;
    }


    @Step
    public String sendOTPRequestToInvalidMobileNumber(){
        String json = SendOTPRequestDataBuilder.buildOTPRequestJsonWithInvalidNumber();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REQUEST_OTP)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        return response;
    }


    @Step
    public String sendOTPRequestToEmptyDiallingCode(){
        String json = SendOTPRequestDataBuilder.buildOTPRequestJsonWithEmptyDiallingCode();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REQUEST_OTP)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        return response;
    }


    @Step
    public String sendOTPRequestToInvalidDiallingCode(){
        String json = SendOTPRequestDataBuilder.buildOTPRequestJsonWithInvalidDiallingCode();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .when()
                .body(json)
                .post(MVP1Endpoint.REQUEST_OTP)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        return response;
    }


    @Step
    public String sendOTPRequestToRegisterdMobileNumber(){
        String json = SendOTPRequestDataBuilder.buildOTPRequestJsonWithRegisteredMobileNumber();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REQUEST_OTP)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        return response;
    }


    @Step
    public String sendOTPRequestToEmptyMobileNumber(){
        String json = SendOTPRequestDataBuilder.buildOTPRequestJsonWithEmptyMobileNumber();
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REQUEST_OTP)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        return response;
    }

    public void checkEmailVerificationStatus(){
        String response = SerenityRest.rest().specification(requestAccessTokenSpesification)
                .given()
                .log().all()
                .when()
                .get(MVP1Endpoint.EMAIL_VERIFICATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("email_verification"));
    }

    public void failedCredentialsToCheckEmailVerificationStatus(){
        String response = SerenityRest.rest().specification(requestRemoveAccessToken)
                .given()
                .log().all()
                .when()
                .get(MVP1Endpoint.EMAIL_VERIFICATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
    }

    public void checkEmailVerificationStatusWithOldAccessToken(){
        String response = SerenityRest.rest().specification(requestAccessTokenSpesification)
                .given()
                .log().all()
                .when()
                .get(MVP1Endpoint.EMAIL_VERIFICATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
    }

    public void checkEmailVerificationStatusWithNewAccessToken(){
        String response = SerenityRest.rest().specification(requestSpecificationNewAccessToken)
                .given()
                .log().all()
                .when()
                .get(MVP1Endpoint.EMAIL_VERIFICATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("email_verification"));
    }


    @Step
    public String registrationWithValidOTPVerification(Consumer consumer){
        String json = RegisterDataBuilder.resgistrationWithValidOTPVerification(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REGISTRATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("consumer"));
        return response;
    }


    @Step
    public String registrationWithEmptyOTPVerification(Consumer consumer){
        String json = RegisterDataBuilder.resgistrationWithEmptyOTPVerification(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REGISTRATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        JSONArray code = read(response,"$.errors[?(@.code =='-400')]");
        assertThat(JsonUtil.getAttribute(code,MESSAGE),equalTo("The otp field is required."));
        return response;
    }


    @Step
    public String registrationWithInvalidOTPVerification(Consumer consumer){
        String json = RegisterDataBuilder.resgistrationWithInvalidOTPVerification(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REGISTRATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        JSONArray code = read(response,"$.errors[?(@.code =='invalid_otp')]");
        assertThat(JsonUtil.getAttribute(code,MESSAGE),equalTo("Please check if you've entered the correct 6 digit One-Time Password and try again."));
        return response;
    }

    @Step
    public String registrationWithInvalidFormatOTPVerification(Consumer consumer){
        String json = RegisterDataBuilder.registrationWithInvalidFormatOTPVerification(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REGISTRATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        JSONArray code = read(response,"$.errors[?(@.code =='invalid_otp')]");
        assertThat(JsonUtil.getAttribute(code,MESSAGE),equalTo("Please check if you've entered the correct 6 digit One-Time Password and try again."));
        return response;
    }

    @Step
    public String registrationWithExpiredOTPVerification(Consumer consumer){
        String json = RegisterDataBuilder.registrationWithExpiredOTPVerification(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REGISTRATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        JSONArray code = read(response,"$.errors[?(@.code =='invalid_otp')]");
        assertThat(JsonUtil.getAttribute(code,MESSAGE),equalTo("Your One-Time Password has expired. Please tap the 'Send again' button and enter the One-Time Password within 3 minutes."));
        return response;
    }

    @Step
    public String registrationWithRegisteredEmailAddress(Consumer consumer){
        String json = RegisterDataBuilder.registrationWithRegisteredEmailAddress(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REGISTRATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        JSONArray code = read(response,"$.errors[?(@.code =='invalid_email')]");
        assertThat(JsonUtil.getAttribute(code,MESSAGE),equalTo("This email address is already registered with us. Try logging in or use a different email."));
        return response;
    }

    @Step
    public String registrationWithInvalidEmailAddress(Consumer consumer){
        String json = RegisterDataBuilder.registrationWithInvalidEmailAddress(consumer);
        String response = SerenityRest.rest().specification(requestSpecification)
                .given()
                .log().all()
                .when()
                .body(json)
                .post(MVP1Endpoint.REGISTRATION)
                .then()
                .log().all()
                .extract().asString();
        assertThat(from(response).get("type"),equalTo("error"));
        JSONArray code = read(response,"$.errors[?(@.code =='-400')]");
        assertThat(JsonUtil.getAttribute(code,MESSAGE),equalTo("Invalid email format"));
        return response;
    }



}
