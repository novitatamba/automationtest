package com.nvt.automation.steps.mobile;


import com.nvt.automation.model.Card;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.page.*;
import com.nvt.automation.page.*;
import net.thucydides.core.annotations.Step;

/**
 * Created by Thanh Chuong on 23/3/17.
 */
public class ConsumerMobileSteps {

    private StartPage startPage;
    private WelcomePage welcomePage;
    private LoginPage loginPage;
    private PursePage pursePage;
    private CardsPage cardsPage;
    private MainPage mainPage;

    @Step
    public void shouldAbleToLogin(Consumer consumer){
        startPage.goToWelcomePage();
        welcomePage.goToLoginPage();
        loginPage.enterValidCredentials(consumer);
    }

    @Step
    public void shouldAbleToAddNewPurse(){
        mainPage.goToWalletPane();
        pursePage.goToPurseTab();
    }

    @Step
    public void shouldBeAbleToAddNewDinersCard(Card card){
        mainPage.goToWalletPane();
        cardsPage.addNewMasterCard(card);
    }

    @Step
    public void shouldBeAbleToAddMasterCard(Card card){
        mainPage.goToWalletPane();
        cardsPage.addNewMasterCard(card);
    }

    @Step
    public void shouldBeAbleToAddVisaCard(Card card){
        mainPage.goToWalletPane();
        cardsPage.addNewVisaCard(card);
    }

    @Step
    public void selectLiquidCashPurse() throws Exception{
        String desc = "Upfront savings anywhere you buy!";
        mainPage.goToWalletPane();
        pursePage.goToPurseTab();
        pursePage.selectPurse(desc);
    }

    @Step
    public void topUpUsingENETS(long amount){
        pursePage.topUpUsingENETS(amount);
    }


}
