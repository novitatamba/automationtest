package com.nvt.automation.steps.api;

import com.nvt.automation.database.SQLHelperFacade;
import com.nvt.automation.database.SQLType;
import com.nvt.automation.model.Consumer;
import net.thucydides.core.annotations.Step;
import java.sql.*;
import java.util.Random;

/**
 * Created by Thanh Chuong on 23/2/17.
 */
public class DatabaseHelper {

    static final long duration = 3 * 60 * 1000;
    static final long durationLogin =  15 * 60 * 1000;


    @Step
    public void changeMobileToRandomIfExist(String mobile) throws Exception{
        String userID = getUserIdByMobile(mobile);
        if(userID!=null){
            updateMobile(userID, getRandomMobile());
        }
    }

    @Step
    public void changeMobileNumber(Consumer user, String newMobile) throws Exception{
        String currentUserID = getUserId(user);
        String oldUserId = getUserIdByMobile(newMobile);
        String currentMobile = getMobileByEmail(user.getEmail());

        if(currentUserID.equals(oldUserId)){
            return;
        }

        if(oldUserId!=null){
            updateMobile(oldUserId, getRandomMobile());
            updateMobile(currentUserID, newMobile);
            updateMobile(oldUserId, currentMobile);
        } else {
            updateMobile(currentUserID, newMobile);
        }
    }

    @Step
    public void deleteNumberIfExist(String number) throws Exception{
            deleteMobileFromCustomerOTPTable(number);
    }

    private void deleteMobileFromCustomerOTPTable(String number) throws Exception{
        String updateSQL = "delete from CUSTOMER_OTP where MOBILE=?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(updateSQL);
            preparedStatement.setString(1,String.valueOf(number));
            preparedStatement.executeUpdate();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Step
    public void deleteUserIfExist(Consumer user) throws Exception{
        String userID = getUserId(user);
        if(userID!=null){
            deleteUserFromCustomerDetailsTable(userID);
            deleteUserFromCustomerSVCTable(userID);
            deleteUserFromCustomerLoginTable(userID);
        }
    }

    private void deleteUserFromCustomerDetailsTable(String userId) throws Exception{
        String updateSQL = "delete from CUSTOMER_DETAILS where CUSTNO=?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(updateSQL);
            preparedStatement.setInt(1, Integer.valueOf(userId));
            preparedStatement.executeUpdate();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    @Step
    public void updateCustomerOTP(String number) throws Exception{
        updateCustomerOTPFromCustomerOTPTable(number);
    }


    private void updateCustomerOTPFromCustomerOTPTable(String number) throws Exception{
        Timestamp lastupdated = getLASTUPDATED(number);
        lastupdated.setTime(lastupdated.getTime() - duration);
        String updateSQL = "UPDATE CUSTOMER_OTP SET LASTUPDATED =? WHERE MOBILE =?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(updateSQL);
            preparedStatement.setTimestamp(1,lastupdated);
            preparedStatement.setString(2, String.valueOf(number));
            preparedStatement.executeUpdate();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Timestamp getLASTUPDATED (String number) {
        String query = "Select LASTUPDATED  from CUSTOMER_OTP where MOBILE like ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, number);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return resultSet.getTimestamp(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    private void deleteUserFromCustomerLoginTable(String userId) throws Exception{
        String updateSQL = "delete from CUSTOMER_LOGIN where CUSTNO=?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(updateSQL);
            preparedStatement.setInt(1, Integer.valueOf(userId));
            preparedStatement.executeUpdate();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void deleteUserFromCustomerSVCTable(String userId) throws Exception{
        String updateSQL = "delete from CUSTOMER_SVC where CUSTNO=?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(updateSQL);
            preparedStatement.setInt(1, Integer.valueOf(userId));
            preparedStatement.executeUpdate();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void updateMobile(String userId, String mobile) throws Exception{
        String updateSQL = "update CUSTOMER_DETAILS set MOBILE=? where CUSTNO=?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(updateSQL);
            preparedStatement.setString(1, mobile);
            preparedStatement.setInt(2, Integer.valueOf(userId));
            preparedStatement.executeUpdate();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getMobileByEmail(String email){
        String query = "select MOBILE from CUSTOMER_DETAILS d INNER JOIN CUSTOMER_LOGIN l " +
                "on d.CUSTNO=l.CUSTNO where EMAIL like ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return resultSet.getString(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private String getUserId(Consumer user) {
        String query = "Select CUSTNO from CUSTOMER_LOGIN where EMAIL like ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, user.getEmail());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return resultSet.getString(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private String getUserIdByMobile(String mobile) {
        String query = "Select CUSTNO from CUSTOMER_DETAILS where MOBILE = ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, mobile);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return resultSet.getString(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Step
    public void updateExpiryDate(String emailAddress) throws Exception{
        updateCustomerExpiryDate(emailAddress);
    }

    private void updateCustomerExpiryDate(String emailAddress) throws Exception{
        Timestamp lastupdated = getExpiredDate(emailAddress);
        lastupdated.setTime(lastupdated.getTime() - durationLogin);
        String updateSQL = "update (select EXPIRYDATE from CUSTOMER_AUTHENTICATION auth INNER JOIN CUSTOMER_LOGIN login " + " on auth.custno = login.custno where email =?) u set u.expirydate =?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(updateSQL);
            preparedStatement.setString(1, String.valueOf(emailAddress));
            preparedStatement.setTimestamp(2,lastupdated);
            preparedStatement.executeUpdate();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Timestamp getExpiredDate (String email) {
        String query = "select max(Expirydate) from CUSTOMER_AUTHENTICATION a INNER JOIN CUSTOMER_LOGIN l " + " on a.CUSTNO=l.CUSTNO where l.EMAIL like ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection con = SQLHelperFacade.getConnection(SQLType.ORACLE);
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return resultSet.getTimestamp(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private String getRandomMobile(){
        while (true){
            String mobile = generateRandomNumber();
            if(getUserIdByMobile(mobile)==null){
                return mobile;
            }
        }
    }

    private String generateRandomNumber(){
        Random rand = new Random();
        int  n = rand.nextInt(9000000) + 40000000;
        return String.valueOf(n);
    }

}
