package com.nvt.automation.steps.api;

import com.nvt.automation.web.util.SerenityUtil;

/**
 * Created by Thanh Chuong on 14/2/17.
 */
public class StepsUtil {
    public static String getAPIURL(){
        return SerenityUtil.getEnv("api.url");
    }
}
