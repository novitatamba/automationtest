package com.nvt.automation.steps.api;

import com.nvt.automation.constants.MerchantWSEndPoint;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.model.Merchant;
import com.nvt.automation.model.PromoCode;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by Thanh Chuong on 10/3/17.
 */
public class MerchantSteps {
    private String SESSIONKEY;
    private String USERNAME;
    private String BILLREFNO;
    private ConsumerSteps consumersteps = new ConsumerSteps();
    Consumer consumer;


    @Step
    public void login(Merchant user){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERNAME", user.getUserName())
                .param("PASSWORD", user.getPassword())
                .param("DEVICEID", user.getDeviceId())
                .param("DEVICEUID", user.getDeviceUID())
                .param("DEVICETYPE", user.getDeviceType())
                .when()
                .post(StepsUtil.getAPIURL()+ MerchantWSEndPoint.LOGIN)
                .then().extract().asString();
        USERNAME = JsonPath.from(response).getString("DATALIST[0].USERNAME");
        SESSIONKEY = JsonPath.from(response).getString("DATALIST[0].SESSIONKEY");
    }

    @Step
    public PromoCode getPromoCode(){
        PromoCode promoCode = new PromoCode();
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERNAME", USERNAME)
                .param("SESSIONKEY", SESSIONKEY)
                .when()
                .post(StepsUtil.getAPIURL()+ MerchantWSEndPoint.GET_PROMO_CODE)
                .then().extract().asString();
        promoCode.setCode(JsonPath.from(response).getString("DATALIST[0].PROMOCODE"));
        promoCode.setFactor(JsonPath.from(response).getDouble("DATALIST[0].FACTOR"));
        promoCode.setPreviousMonthCount(JsonPath.from(response).getInt("DATALIST[0].PREVIOUSMONTHCOUNT"));
        promoCode.setCurrentMonthCount(JsonPath.from(response).getInt("DATALIST[0].CURRENTMONTHCOUNT"));
        return promoCode;
    }

    @Step("Number of new added subs should be {2}")
    public void shouldUpdatePromoCodeCorrectly(PromoCode prePromoCode, PromoCode currPromoCode, int numOfNewSub){
        assertThat(currPromoCode.getCurrentMonthCount(), equalTo(prePromoCode.getCurrentMonthCount()+numOfNewSub));
        assertThat(currPromoCode.getPreviousMonthCount(), equalTo(prePromoCode.getPreviousMonthCount()));
    }

    @Step
    public String send_open_bill(){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERNAME",USERNAME)
                .param("SESSIONKEY",SESSIONKEY)
                .param("BILLREFNO",consumersteps.getRequestBill())
                .param("DESTINATIONAMOUNT","10.00")
                .param("IMAGE","")
                .param("CUSTNO",consumersteps.getUSERID())
                .param("CURRENCYCODE","SGD")
                .when()
                .post(StepsUtil.getAPIURL()+MerchantWSEndPoint.SEND_OPEN_BILL)
                .then().extract().asString();

       /* String response2 = SerenityRest.rest()
                .given()
                .multiPart(new File("C:\\Users\\Liquidpay\\Pictures\\test.jpg"))
                .when()
                .post(StepsUtil.getAPIURL()+MerchantWSEndPoint.OPEN_BILL)
                .then().extract().asString();*/

        return response;
    }
}
