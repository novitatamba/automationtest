package com.nvt.automation.steps.api;


import com.nvt.automation.constants.ConsumerWSEndPoint;
import com.nvt.automation.model.AccountStatus;
import com.nvt.automation.model.Card;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.model.Purse;
import com.nvt.automation.web.util.EmailChecker;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by Thanh Chuong on 14/2/17.
 */
public class ConsumerSteps {
    Consumer user;
    private String USERID;
    private String SESSIONKEY;
    private AccountStatus ACCOUNT_STATUS;
    private String FIRST_NAME;
    private String LAST_NAME;
    private String GENDER;
    private String EMAIL;
    private String BIRTHDATE;
    private String MOBILE;
    private String DIALINGCODE;
    private String DIALINGCOUNTRYCODE;
    private String VERIFICATION_LINK;
    private String PAYMENTCODE;
    private String BILLREFNO;

    private EmailHelper emailHelper = new EmailHelper();
    private EmailChecker emailChecker = new EmailChecker();

    public void login(Consumer user, String password){
        if(password==null){
            password = user.getPassword();
        }
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.ANY)
                .param("EMAIL", user.getEmail())
                .param("PASSWORD", password)
                .param("DEVICEID", user.getDeviceId())
                .param("DEVICEUID", user.getDeviceUID())
                .param("DEVICEMODEL", user.getDeviceModel())
                .param("DEVICEOS", user.getDeviceOS())
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.LOGIN)
                .then().extract().asString();
        USERID = JsonPath.from(response).getString("DATALIST[0].USERID");
        SESSIONKEY = JsonPath.from(response).getString("DATALIST[0].SESSIONKEY");
        FIRST_NAME = JsonPath.from(response).getString("DATALIST[0].NAME");
        LAST_NAME = JsonPath.from(response).getString("DATALIST[0].SURNAME");
        ACCOUNT_STATUS =AccountStatus.valueOf(JsonPath.from(response).getString("DATALIST[0].STATUS"));
    }

    @Step
    public void requestForResendingEmailVerification(Consumer user){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("NEWEMAIL", user.getEmail())
                .param("OLDEMAIL", user.getEmail())
                .param("PASSWORD", user.getPassword())
                .log().all()
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.UPDATE_EMAIL)
                .then()
                .log().all()
                .extract().asString();
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    @Step
    public void shouldBeAbleToRequestOTP(Consumer user){
        String response = requestOTP(user);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    @Step
    public void shouldBeUnableToRegisterWithInvalidEmail(Consumer user){
        String errorMsg = "Invalid input parameters";
        user.setEmail("");
        String response = validateInfo(user, "");
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-4));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step
    public Card updateCardDetails(Card card){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
//                .param("CARDBIN", card.getCardNo())
                .when()
                .post(StepsUtil.getAPIURL()+ String.format(ConsumerWSEndPoint.GET_CARD_INFO,card.getCardNo()))
                .then().extract().asString();
        card.setCardBrand(JsonPath.from(response).getString("DATALIST[0].BRANDID"));
        card.setCardCategory(JsonPath.from(response).getString("DATALIST[0].CARDCATEGORY"));
        card.setCardType(JsonPath.from(response).getString("DATALIST[0].CARDTYPE"));
        card.setCurrency(JsonPath.from(response).getString("DATALIST[0].CURRENCY"));
        card.setIssueBank(JsonPath.from(response).getString("DATALIST[0].BANK"));
        return card;
    }

    @Step
    public void shouldBeAbleToAddCard(Card card){
        String response = addCard(card);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    private String addCard(Card card){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID", USERID)
                .param("SESSIONKEY", SESSIONKEY)
                .param("NAME", card.getName())
                .param("CARDNO", card.getCardNo())
                .param("EXPIRY", card.getExpiry())
                .param("CVV", card.getCvv())
                .param("CURRENCY", card.getCurrency())
                .param("CARDTYPE", card.getCardType())
                .param("CARDBRAND", card.getCardBrand())
                .param("DEFAULTCARD", card.getDefaultCard())
                .param("ISSUERBANK", card.getIssueBank())
                .param("COLOR", card.getColor())
                .param("CARDCATEGORY", card.getCardCategory())
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.ADD_CARD)
                .then().extract().asString();
        return response;
    }

    private String requestOTP(Consumer user){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("EMAIL",user.getEmail())
                .param("DIALINGCODE",user.getDialingCode())
                .param("MOBILE",user.getNumber())
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.SEND_OTP)
                .then().extract().asString();
        return response;
    }

    private String validateInfo(Consumer user, String promoCode){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("EMAIL",user.getEmail())
                .param("PROMOCODE",promoCode)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.VALIDATE_INFO)
                .then().extract().asString();
        return response;
    }

    @Step
    public void deleteCardIfExist(Card card){
        String response = getWalletInfo();
        List<Map<String, String>> cards = (List) JsonPath.from(response).getList("DATALIST[0].LISTCARDS");
        for(Map<String,String> item: cards){
            if(item.get("CARDNAME").equalsIgnoreCase(card.getName())){
                card.setCardGUID(item.get("CARDGUID"));
                shouldBeAbleToDeleteCard(card);
            }
        }
    }

    @Step
    public void shouldBeAbleToDeleteCard(Card card){
        String response = deleteCardByGUIID(card.getCardGUID());
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    private String deleteCardByGUIID(String id){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("CARDGUID",id)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.DELETE_CARD)
                .then().extract().asString();
        return response;

    }

    @Step
    public void shouldBeUnableToRegisterWithInvalidPromoCode(Consumer user, String promoCode){
        String errorMsg = "The promo code cannot be found, please enter a valid promo code.";
        String response = validateInfo(user, promoCode);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-1036));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step
    public void getUserDetailInformation(){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.GET_DETAILS)
                .then().extract().asString();
        GENDER = JsonPath.from(response).getString("DATALIST[0].GENDER");
        EMAIL = JsonPath.from(response).getString("DATALIST[0].EMAIL");
        FIRST_NAME = JsonPath.from(response).getString("DATALIST[0].NAME");
        LAST_NAME = JsonPath.from(response).getString("DATALIST[0].SURNAME");
        BIRTHDATE = JsonPath.from(response).getString("DATALIST[0].BIRTHDATE");
        MOBILE = JsonPath.from(response).getString("DATALIST[0].MOBILE");
        DIALINGCODE = JsonPath.from(response).getString("DATALIST[0].DIALINGCODE");
        DIALINGCOUNTRYCODE = JsonPath.from(response).getString("DATALIST[0].DIALINGCOUNTRYCODE");

    }

    @Step
    public void verifyThatUserCanRegister(Consumer user, String otp, String promoCode){
        String response = register(user, otp, promoCode);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
        //Make sure the account's status is Pending
        assertThat(JsonPath.from(response).getString("DATALIST[0].STATUS"), equalTo("P"));
    }


    @Step
    public void verifyThatUserCanNotRegisterWithEmptyEmail(Consumer user, String promoCode){
        String errorMsg = "Invalid input parameters";
        user.setEmail("");
        String response = validateInfo(user, promoCode);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-4));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step
    public void verifyThatUserCanNotRegisterWithoutFirstName(Consumer user, String otp, String promoCode){
        String errorMsg = "Invalid input parameters";
        user.setFirstName("");
        String response = register(user, otp, promoCode);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-4));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step
    public void verifyThatUserCanNotRegisterWithoutLastName(Consumer user, String otp, String promoCode){
        String errorMsg = "Invalid input parameters";
        user.setNewLastName("");
        String response = register(user, otp, promoCode);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-4));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }


    @Step
    public void verifyThatUserCanNotRegisterWithoutFirstNameAndLastName(Consumer user, String otp, String promoCode){
        String errorMsg = "Invalid input parameters";
        user.setFirstName("");
        user.setLastName("");
        String response = register(user, otp, promoCode);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-4));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }


    @Step
    public void verifyThatUserCannotRegisterWithEmptyPassword(Consumer user, String otp, String promoCode){
        user.setPassword("");
        String response = register(user, otp, promoCode);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-4));
    }

    @Step("Verify that user can't register with invalid password : {3} ")
    public void verifyThatUserCannotRegisterWithInvalidPassword(Consumer user, String otp, String promoCode, String pwd){
        user.setPassword(pwd);
        String response = register(user, otp, promoCode);
        assertThat("User can register with invalid password, ",
                JsonPath.from(response).getInt("CODE"), not(1));
    }

    @Step("Register with email: {0}, OTP: {1}, promo code: {2}")
    public String register(Consumer user, String otp, String promoCode){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("EMAIL",user.getEmail())
                .param("DIALINGCODE",user.getDialingCode())
                .param("DIALINGCOUNTRYCODE",user.getDialingCountryCode())
                .param("MOBILE",user.getNumber())
                .param("OTP",otp)
                .param("FIRSTNAME",user.getFirstName())
                .param("LASTNAME",user.getLastName())
                .param("PASSWORD",user.getPassword())
                .param("DEVICEID",user.getDeviceId())
                .param("DEVICEUID",user.getDeviceUID())
                .param("DEVICEMODEL",user.getDeviceModel())
                .param("DEVICEOS",user.getDeviceOS())
                .param("PROMOCODE",promoCode)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.REGISTRATION)
                .then().extract().asString();
        return response;

    }

    @Step
    public void login(Consumer user){
        this.user = user;
        login(user, null);
    }

    private void verifyThatAccountStatusIs(AccountStatus accountStatus){
        assertThat(ACCOUNT_STATUS, equalTo(accountStatus));
    }

    @Step()
    public void accountStatusShouldBeActive(){
        verifyThatAccountStatusIs(AccountStatus.A);
    }

    @Step("Verify that account status is pending")
    public void verifyThatAccountStatusIsPending(){
        verifyThatAccountStatusIs(AccountStatus.P);
    }

    @Step
    public void verifyThatCustomerCanLoginWithNewPassword(Consumer user, String newPassword){
        //If no error occurs, that means user can login with the new password.
        // Other wise it will thrown an exception
        login(user, newPassword);
    }

    @Step
    public void changePasswordTo(String oldPwd,String newPwd){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("OLDPASSWORD",oldPwd)
                .param("NEWPASSWORD",newPwd)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.CHANGE_PWD)
                .then().extract().asString();
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    private String activateAccount(String nationality, String idNo){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("IDNO",idNo)
                .param("NATIONALITY",nationality)
                .param("COUNTRYOFRESIDENT","")
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.ACTIVATE_ACC)
                .then().extract().asString();
        return response;
    }

    @Step
    public void shouldBeUnableToActivateLiquidCashUsingRegisteredID(String nationality, String idNo){
        String errorMsg = "This NRIC is already registered with us. Try logging in or contact contact@liquidpay.com for more information.";
        String response = activateAccount(nationality, idNo);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-8));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));

    }

    @Step
    public void logOut(){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.LOGOUT)
                .then().extract().asString();
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    @Step("Transfer cash to: {0}, {1}")
    public String transferCashTo(Consumer recipient, double amount){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("SOURCECUSTNO",USERID)
                .param("TRANSFERAMT", amount)
                .param("DESTMOBILE",recipient.getNumber())
                .param("STOREVALUETYPE","LQDSV")
                .param("MERCHANTID","LIQUID")
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.TRANSFER_CASH)
                .then().extract().asString();
        return response;
    }

    @Step()
    public void isNotAllowedToTransferCashToPPUser(Consumer recipient, double amount){
        String errorMsg = "Sorry, only SG citizens or SG residents are allowed to receive Liquid Cash.";
        String response = transferCashTo(recipient, amount);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-4));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step()
    public void isNotAllowedToTransferInvalidAmount(Consumer recipient, double amount){
        String errorMsg = "Invalid input format, please transfer valid amount.";
        String response = transferCashTo(recipient, amount);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-8));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step()
    public void isNotAllowToTransferWithExceededMaximumAmountPerTransaction(Consumer recipient, double amount){
        String errorMsg = "You have exceeded the maximum transfer amount per transaction. Please enter a lower number.";
        String response = transferCashTo(recipient, amount);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-603));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step()
    public void isNotAllowToTransferMoreThanCurrentBalance(Consumer recipient, double amount){
        String errorMsg = "Dear customer, your Liquid Cash does not have sufficient funds. Please Top-up first to complete your transfer.";
        String response = transferCashTo(recipient, amount);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-601));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step()
    public void isNotAllowToTransferToOwnMobileNumber(Consumer recipient, double amount){
        String errorMsg = "Invalid request, it is not allowed to transfer Liquid Cash to your own mobile number.";
        String response = transferCashTo(recipient, amount);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-4));
        assertThat(JsonPath.from(response).getString("DESCRIPTION").trim(), equalTo(errorMsg));
    }

    @Step()
    public void isNotAllowToTransferToInvalidMobileNumber(Consumer recipient, double amount){
        String errorMsg = "Mobile number is invalid";
        String response = transferCashTo(recipient, amount);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-1030));
        assertThat(JsonPath.from(response).getString("DESCRIPTION").trim(), equalTo(errorMsg));
    }

    @Step
    public void verifyEmailAccount(){
        String response = SerenityRest.rest()
                .get(VERIFICATION_LINK)
                .then().extract().asString();
        assertThat(response,containsString("Thank you, you have successfully verified your account!"));
    }

    public String getWalletInfo(){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.GET_WALLET)
                .then().extract().asString();
        return response;
    }

    @Step
    public void addPurseIfNotExist(Purse purse){
        String walletInfo = getWalletInfo();
        List<Map<String, String>> purses = (List) JsonPath.from(walletInfo).getList("DATALIST[0].LISTPURSES");
        for(Map<String,String> item: purses){
            if(item.get("MERCHANTID").equalsIgnoreCase(purse.getMerchantId())){
                return;
            }
        }
        String updateProfileResponse = updateProfile(purse);
        assertThat(JsonPath.from(updateProfileResponse).getInt("CODE"), equalTo(1));
        String addPurseResponse = addPurse(purse);
        assertThat(JsonPath.from(addPurseResponse).getInt("CODE"), equalTo(1));
    }

    @Step
    public void shouldBeAbleToAddPurse(Purse purse){
        String updateProfileResponse = updateProfile(purse);
        assertThat(JsonPath.from(updateProfileResponse).getInt("CODE"), equalTo(1));
        String addPurseResponse = addPurse(purse);
        assertThat(JsonPath.from(addPurseResponse).getInt("CODE"), equalTo(1));
    }

    @Step
    public void purseStatusShouldBeInActive(Purse purse) throws Exception{
        String walletInfo = getWalletInfo();
        List<Map<String, String>> purses = (List) JsonPath.from(walletInfo).getList("DATALIST[0].LISTPURSES");
        for(Map<String,String> item: purses){
            if(item.get("MERCHANTID").equalsIgnoreCase(purse.getMerchantId())){
                Object a =  item.get("PURSESTATUS");
                List<Map<String,String>> list = (List<Map<String,String>> )a;
                String cardStatus = list.get(0).get("CARDSTATUS");
                assertThat(cardStatus, equalTo("N"));
                String transferStatus = list.get(0).get("TRANSFERSTATUS");
                assertThat(transferStatus,equalTo("N"));
            }
        }
    }

    @Step
    public void deletedPurseShouldBeGone(Purse deletedPurse){
        String walletInfo = getWalletInfo();
        List<Map<String, String>> purses = (List) JsonPath.from(walletInfo).getList("DATALIST[0].LISTPURSES");
        for(Map<String,String> item: purses){
            if(item.get("MERCHANTID").equalsIgnoreCase(deletedPurse.getMerchantId())){
                Assert.fail("Deleted purse is still in the list");
            }
        }
    }

    private String updateProfile(Purse purse){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("GENDER",user.getGender())
                .param("BIRTHDATE",user.getBirthDate())
                .param("MERCHANTID",purse.getMerchantId())
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.UPDATE_PROFILE)
                .then().extract().asString();
        return response;
    }

    private String addPurse(Purse purse){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("MERCHANTID",purse.getMerchantId())
                .param("STOREVALUETYPE",purse.getStoreValueType())
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.ADD_PURSE)
                .then().extract().asString();
        return response;
    }

    private String deletePurse(Purse purse){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("STOREVALUETYPE",purse.getStoreValueType())
                .param("MERCHANTID",purse.getMerchantId())
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.DELETE_PURSE)
                .then().extract().asString();
        return response;
    }

    @Step
    public void shouldBeAbleToDeletePurse(Purse purse){
        String response =deletePurse(purse);
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    @Step
    public void updateNotificationList(){
        String firstUnreadNo = getFirstUnreadNotification();
        if(firstUnreadNo!=null){
            String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.UPDATE_NOTIFICATION_LIST)
                .then()
                .extract().asString();
            assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
        }
    }

    public String getFirstUnreadNotification(){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("TYPE","UNREAD")
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.GET_NOTIFICATION_LIST)
                .then()
                .extract().asString();
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
        try{
            return JsonPath.from(response).getString("DATALIST[0].CONTENT");
        } catch (Exception ex){
            return null;
        }
    }

    @Step
    public void verifyThatUserReceiveExpectedNotification(String expectedNotification) throws Exception{
        String actualNotification=null;
        for (int i = 0; i < 12; i++) {
            actualNotification = getFirstUnreadNotification();
            if(actualNotification!=null){
                break;
            }
            Thread.sleep(5000);
        }
        assertThat(actualNotification, equalTo(expectedNotification));
    }

    @Step("Get current balance of {0}")
    public double getCurrentBalance(Consumer user){
        String walletInfo = getWalletInfo();
        return Double.valueOf(JsonPath.from(walletInfo).getString("DATALIST[0].LISTPURSES[0].BALANCE"));
    }

    @Step("Verify that current balance should update correctly, actual : {0}, expected : {1}")
    public void verifyThatCurrentBalanceShouldUpdateCorrectly(double actual, double expected){
        assertThat(actual, equalTo(expected));
    }

    @Step
    public void verifyThatSystemShouldPreventUserUseObsoleteSessionKey(){
        String responseFromGetWalletInfo = getWalletInfo();
        assertThat(JsonPath.from(responseFromGetWalletInfo).getInt("CODE"), equalTo(-500));
    }

    @Step
    public void cleanSMSMailBox() throws Exception{
        emailHelper.cleanSMSMailBox();
    }

    @Step
    public void cleanMailBox(Consumer consumer) throws Exception{
        emailHelper.cleanMailBox(consumer);
    }

    @Step
    public void shouldReceiveNewCardAddedConfirmation(Card card) throws Exception{
        String lastFourDigit = card.getCardNo().substring(card.getCardNo().length() - 4);
        String pattern = ".*You have added a card ending with "+lastFourDigit+" to your Liquid Pay account.*";
        emailChecker.checkMail(user.getEmail(),user.getGmailPassword(),"inbox","","",pattern,1);
    }

    @Step
    public void shouldReceiveVerificationLink(Consumer consumer) throws Exception{
        VERIFICATION_LINK = emailHelper.verifyThatCustomerReceivesVerificationEmail(consumer);
    }

    @Step("Current balance of {0} should update to {1}")
    public void shouldUpdateCurrentBalanceTo(Consumer user,double newBalance){
        assertThat(getCurrentBalance(user), equalTo(newBalance));
    }

    @Step
    public void requestPayment(String merchantID, String OutletID, double amount){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("MERCHANTID",merchantID)
                .param("OUTLETID",OutletID)
                .param("AMOUNT",amount)
                .param("LOCNO","")
                .param("PAYMODE","SP")
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.REQUEST_PAYMENT)
                .then().extract().asString();
        PAYMENTCODE = JsonPath.from(response).getString("DATALIST[0].PAYMENTCODE");
    }

    private String payByCard(String id){
        String response = SerenityRest.rest()
                .given()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("PAYMENTCODE",PAYMENTCODE)
                .param("CARDGUID",id)
                .when()
                .post(StepsUtil.getAPIURL()+ConsumerWSEndPoint.PAY_BYCARD)
                .then().extract().asString();
        return response;
    }

    public String payBySVC()
    {
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("PAYMENTCODE",PAYMENTCODE)
                .param("ISSUERBANK","LIQUID")
                .param("STOREVALUETYPE","LQDSV")
                .when()
                .post(StepsUtil.getAPIURL()+ConsumerWSEndPoint.PAY_BYSVC)
                .then().extract().asString();
        return response;

    }

    @Step
    public void checkWhetherCardExisting(Card card){
        String response = getWalletInfo();
        List<Map<String,String>> cards = (List) JsonPath.from(response).getList("DATALIST[0].LISTCARDS");
        for (Map<String,String>item:cards){
            if(item.get("CARDGUID").equalsIgnoreCase(card.getCardNo())){
                shouldBeAbleToPayByCard(card);
            }
        }
    }

    @Step
    public void notExistCard(Card card){
        String response = payByCard(card.getCardNo());
        String errorMsg = "We currently cannot process your payment request. Please contact contact@liquidpay.com if the problem persists.";
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(-100));
        assertThat(JsonPath.from(response).getString("DESCRIPTION"), equalTo(errorMsg));
    }

    @Step
    public void shouldBeAbleToPayByCard(Card card){
        String response = payByCard(card.getCardNo());
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    @Step
    public void shouldBeAblePayBySVC(){
        String response = payBySVC();
        assertThat(JsonPath.from(response).getInt("CODE"),equalTo(1));
    }

    public String updateDetails(String newFirst, String newLast){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param(FIRST_NAME,newFirst)
                .param(LAST_NAME,newLast)
                .param("EMAIL",EMAIL)
                .param("MOBILE",MOBILE)
                .param("DIALINGCODE",DIALINGCODE)
                .param("DIALINGCOUNTRYCODE",DIALINGCOUNTRYCODE)
                .when()
                .post(StepsUtil.getAPIURL()+ ConsumerWSEndPoint.UPDATE_MEMBER)
                .then().extract().asString();
        return response;
    }

    public void requestBill (String merchantID, String outletID){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("MERCHANTID",merchantID)
                .param("OUTLETID",outletID)
                .param("LOCNO","7")
                .param("PAYMODE","PAT")
                .when()
                .post(StepsUtil.getAPIURL()+ConsumerWSEndPoint.REQUEST_BILL)
                .then().extract().asString();
        BILLREFNO = JsonPath.from(response).getString("DATALIST[0].BILLREFNO");
    }

    public String openBill (){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .when()
                .post(StepsUtil.getAPIURL()+ConsumerWSEndPoint.OPEN_BILL)
                .then().extract().asString();

        PAYMENTCODE = JsonPath.from(response).getString("DATALIST[0].PAYMENTCODE");

        return response;

    }

    @Step
    public void notAllowedToPayWithInsufficientBalance()
    {
        String response = payBySVC();
        assertThat(JsonPath.from(response).getInt("CODE"),equalTo(-1014));
    }

    public void topUpSVCUsingLiquidCash(){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("MERCHANTID","spinellicoffee")
                .param("TOPUPAMT","10.00")
                .param("COST","0.0")
                .param("STOREVALUETYPE","MSVC")
                .when()
                .post(StepsUtil.getAPIURL()+ConsumerWSEndPoint.TOPUP_SVC)
                .then().extract().asString();
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    public String topUpSVCUsingCard(String cardguid){
        String response = SerenityRest.rest()
                .given()
                .contentType(ContentType.URLENC)
                .param("USERID",USERID)
                .param("SESSIONKEY",SESSIONKEY)
                .param("MERCHANTID","spinellicoffee")
                .param("CARDGUID",cardguid)
                .param("TOPUPAMT","10.00")
                .param("COST","0.0")
                .param("STOREVALUETYPE","MSVC")
                .when()
                .post(StepsUtil.getAPIURL()+ConsumerWSEndPoint.TOPUP_SVC_CARD)
                .then().extract().asString();
        return response;
    }

    @Step
    public void shouldBeAbleTopUpByCard(Card card){
        String response = topUpSVCUsingCard(card.getCardNo());
        assertThat(JsonPath.from(response).getInt("CODE"), equalTo(1));
    }

    public String getPaymentCode()
    {
        return PAYMENTCODE;
    }

    public String getRequestBill(){return BILLREFNO;}

    public String getUSERID(){return USERID;}

    public String getFullName(){
        return FIRST_NAME + " " + LAST_NAME;
    }
}
