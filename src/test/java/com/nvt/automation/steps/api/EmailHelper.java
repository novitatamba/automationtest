package com.nvt.automation.steps.api;

import com.nvt.automation.model.Consumer;
import com.nvt.automation.web.util.EmailChecker;
import com.nvt.automation.web.util.SerenityUtil;
import net.thucydides.core.annotations.Step;

/**
 * Created by Thanh Chuong on 24/2/17.
 */
public class EmailHelper {
    EmailChecker emailChecker = new EmailChecker();
    private final String email = SerenityUtil.getEnv("sms.email.user");
    private final String pwd = SerenityUtil.getEnv("sms.email.pwd");

    @Step
    public String getOTP() throws Exception{
        String pattern = "Your One-Time Password is";
        emailChecker.checkMail(email, pwd, "sms", "", "", pattern,1);
        String otp = emailChecker.getMessageInfo(pattern+" (\\d*)",1);
        return otp;
    }

    @Step
    public void cleanSMSMailBox() throws Exception{
        emailChecker.cleanInbox(email, pwd, "sms");
    }

    @Step
    public void cleanMailBox(Consumer user) throws Exception{
        emailChecker.cleanInbox(user.getEmail(), user.getGmailPassword(), "Inbox");
    }

    @Step
    public String verifyThatCustomerReceivesVerificationEmail(Consumer user) throws Exception{
        String pattern = "please verify your email address";
        emailChecker.checkMail(user.getEmail(), user.getGmailPassword(),
                "Inbox","","",pattern, 2);
        String link = emailChecker.getMessageInfo("(https:\\/\\/\\S*verify_account\\S*)'",1);
        return link;
    }
}
