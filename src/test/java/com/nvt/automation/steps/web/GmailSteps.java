package com.nvt.automation.steps.web;

import com.nvt.automation.page.GmailHomePage;
import net.thucydides.core.annotations.Step;

/**
 * Created by Thanh Chuong on 27/7/17.
 */
public class GmailSteps {

    GmailHomePage homePage;

    @Step
    public void loginWithValidCredential(){
        homePage.goToHomePage();
    }

    @Step
    public void enterValidCredential(String username, String password){
        homePage.enterCredential(username,password);
    }
}
