package com.nvt.automation.page;

import com.nvt.automation.web.pageobject.MyPageObject;
import net.serenitybdd.core.annotations.findby.By;

/**
 * Created by Thanh Chuong on 27/3/17.
 */
public class MainPage extends MyPageObject {
    private static String WALLET_TAB_XPATH = "//*[@text='Wallet']";

    public void goToWalletPane(){
        clickWhenReady(By.xpath(WALLET_TAB_XPATH));
    }
}
