package com.nvt.automation.page;

import com.nvt.automation.web.pageobject.MyPageObject;
import net.serenitybdd.core.annotations.findby.By;

/**
 * Created by Thanh Chuong on 23/3/17.
 */
public class WelcomePage extends MyPageObject {

    private static final String LOGIN_BUTTON_ID = "com.korvac.liquid.dev:id/button_login";

    public void goToLoginPage(){
        element(By.id(LOGIN_BUTTON_ID)).click();
    }
}
