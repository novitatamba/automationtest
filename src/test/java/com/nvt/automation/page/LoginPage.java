package com.nvt.automation.page;

import com.nvt.automation.model.Consumer;
import com.nvt.automation.web.pageobject.MyPageObject;
import net.serenitybdd.core.annotations.findby.By;

/**
 * Created by Thanh Chuong on 23/3/17.
 */
public class LoginPage extends MyPageObject {

    private static final String EMAIL_INPUT_ID = "com.korvac.liquid.dev:id/et_email";
    private static final String PWD_INPUT_ID = "com.korvac.liquid.dev:id/et_password";
    private static final String LOGIN_BUTTON_ID = "com.korvac.liquid.dev:id/btn_login";
    private static final String SKIP_TUTOR_BUTTON_ID = "com.korvac.liquid.dev:id/text_view_skip";


    public void enterValidCredentials(Consumer consumer){
        element(By.id(EMAIL_INPUT_ID)).sendKeys(consumer.getEmail());
        element(By.id(PWD_INPUT_ID)).sendKeys(consumer.getPassword());
        element(By.id(LOGIN_BUTTON_ID)).click();
        clickWhenReady(By.id(SKIP_TUTOR_BUTTON_ID));
    }

}
