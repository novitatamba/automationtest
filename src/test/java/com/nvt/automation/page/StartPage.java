package com.nvt.automation.page;

import com.nvt.automation.web.pageobject.MyPageObject;
import net.serenitybdd.core.annotations.findby.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**Ø
 * Created by Thanh Chuong on 23/3/17.
 */
public class StartPage extends MyPageObject {
    private static final String WELCOME_IMAGE_ID = "com.korvac.liquid.dev:id/iv_image";
    private static final String SKIP_BUTTON_ID = "com.korvac.liquid.dev:id/text_view_skip";

    public void goToWelcomePage(){
        WebDriverWait wait = new WebDriverWait(getDriver(), 180);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(WELCOME_IMAGE_ID)) );
//        wait.until(Function.identity().)
        element(By.id(SKIP_BUTTON_ID)).click();
    }
}
