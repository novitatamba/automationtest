package com.nvt.automation.page;

import com.nvt.automation.web.pageobject.MyPageObject;
import com.nvt.automation.web.util.SerenityUtil;
import org.openqa.selenium.By;

/**
 * Created by Thanh Chuong on 27/7/17.
 */
public class GmailHomePage extends MyPageObject{

    private static final String EMAIL_INPUT_XPATH = "//input[@type='email']";
    private static final String PASSWORD_INPUT_XPATH = "//input[@type='password']";
    private static final String USERNAME_NEXT_BUTTON_ID = "identifierNext";
    private static final String PASSWORD_NEXT_BUTTON_ID = "passwordNext";


    public void goToHomePage(){
        getDriver().get(SerenityUtil.getBaseUrl());
    }

    public void enterCredential(String username, String password){
        find(By.xpath(EMAIL_INPUT_XPATH)).type(username);
        find(By.id(USERNAME_NEXT_BUTTON_ID)).click();
        waitABit(1);
        find(By.xpath(PASSWORD_INPUT_XPATH)).type(password);
        find(By.id(PASSWORD_NEXT_BUTTON_ID)).click();
    }

}
