package com.nvt.automation.page;

import com.nvt.automation.web.pageobject.MyPageObject;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;


/**
 * Created by Thanh Chuong on 23/3/17.
 */
public class PursePage extends MyPageObject {
    private static String PURSES_TAB_XPATH = "//*[@text='PURSES']";
    private static String TOP_UP_XPATH = "//*[@text='Top-up']";

    public void goToPurseTab(){
        find(By.xpath(PURSES_TAB_XPATH)).click();
    }

    public void selectPurse(String description) throws Exception{
        String purseXpath = "//*[contains(@resource-id,'ll_item_card')]";
        List<WebElementFacade> listPurses = findAll(By.xpath(purseXpath));
        for (WebElementFacade purseEl: listPurses){
            purseEl.click();
            WebElementFacade descEl = find(
                    By.xpath(purseXpath+"/../..//*[contains(@resource-id,'tv_item_fragment_purse_promotion_message')]"));
            if (descEl.getText().equalsIgnoreCase(description)){
                return;
            }
        }
        throw new Exception("Cannot find purse with description: "+description);
    }

    public void topUpUsingENETS(long amount){
        clickWhenReady(By.xpath(TOP_UP_XPATH));
        clickWhenReady(By.xpath("//*[@text='+"+amount+"']"));
        clickWhenReady(By.xpath("//*[@text='eNETS']"));
        clickWhenReady(By.id("com.korvac.liquid.dev:id/ll_item_fragment_select_bank_root"));
        clickWhenReady(By.xpath("//*[@text='Proceed']"));
        getAndroidDriver().context("WEBVIEW_com.korvac.liquid.dev");
        clickWhenReady(By.name("submitbutton"));
        getAndroidDriver().context("NATIVE_APP");
        clickWhenReady(By.xpath("//*[@text='OK']"));
    }
}
