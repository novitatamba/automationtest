package com.nvt.automation.page;

import com.nvt.automation.model.Card;
import com.nvt.automation.web.pageobject.MyPageObject;
import net.serenitybdd.core.annotations.findby.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.concurrent.TimeUnit;

/**
 * Created by Thanh Chuong on 27/3/17.
 */
public class CardsPage extends MyPageObject {
    private static final String ADD_CARD_BT_ID = "com.korvac.liquid.dev:id/iv_toolbar_action";
    private static final String ADD_CREDIT_CARD_XPATH = "//*[@text='Credit/Debit Cards']";
    private static final String CARD_NO_INPUT_ID = "com.korvac.liquid.dev:id/et_fragment_add_card_card_no";
    private static final String CARD_EXPIRY_INPUT_ID = "com.korvac.liquid.dev:id/et_fragment_add_card_expiry";
    private static final String CARD_CVC_INPUT_ID = "com.korvac.liquid.dev:id/et_fragment_add_card_cvv";
    private static final String CARD_NAME_INPUT_ID = "com.korvac.liquid.dev:id/et_fragment_add_card_card_name";
    private static final String ADD_THIS_CARD_BT_ID = "com.korvac.liquid.dev:id/btn_fragment_add_card_confirm";
    private static final String CONFIRM_BT_ID = "com.korvac.liquid.dev:id/confirm_button";
    private static final String LOADING_PANE_ID = "com.korvac.liquid.dev:id/content_text";

    public void addNewVisaCard(Card card){
        clickWhenReady(By.id(ADD_CARD_BT_ID));
        clickWhenReady(By.xpath(ADD_CREDIT_CARD_XPATH));
        element(By.id(CARD_NO_INPUT_ID)).sendKeys(card.getCardNo());
        element(By.id(CARD_EXPIRY_INPUT_ID)).sendKeys(card.getExpiry());
        element(By.id(CARD_CVC_INPUT_ID)).sendKeys(card.getCvv());
        element(By.id(CARD_NAME_INPUT_ID)).sendKeys(card.getName());
        getAndroidDriver().hideKeyboard();
        clickWhenReady(By.id(ADD_THIS_CARD_BT_ID));
        withTimeoutOf(60,TimeUnit.SECONDS)
                .waitFor(ExpectedConditions.visibilityOfElementLocated(By.id(LOADING_PANE_ID)));
        shouldContainText(element(By.id(LOADING_PANE_ID))
                ,"We will need to bill your card $5 to verify it." +
                " But don’t worry, it’ll be reversed immediately.");
        clickWhenReady(By.id(CONFIRM_BT_ID));
    }

    public void addNewMasterCard(Card card){
        clickWhenReady(By.id(ADD_CARD_BT_ID));
        clickWhenReady(By.xpath(ADD_CREDIT_CARD_XPATH));
        element(By.id(CARD_NO_INPUT_ID)).sendKeys(card.getCardNo());
        element(By.id(CARD_EXPIRY_INPUT_ID)).sendKeys(card.getExpiry());
        element(By.id(CARD_CVC_INPUT_ID)).sendKeys(card.getCvv());
        element(By.id(CARD_NAME_INPUT_ID)).sendKeys(card.getName());
        getAndroidDriver().hideKeyboard();
        clickWhenReady(By.id(ADD_THIS_CARD_BT_ID));
        withTimeoutOf(60,TimeUnit.SECONDS)
                .waitFor(ExpectedConditions.visibilityOfElementLocated(By.id(LOADING_PANE_ID)));
        shouldContainText(element(By.id(LOADING_PANE_ID))
                ,"We will need to bill your card $5 to verify it." +
                        " But don’t worry, it’ll be reversed immediately.");
        clickWhenReady(By.id(CONFIRM_BT_ID));
        withTimeoutOf(60,TimeUnit.SECONDS)
                .waitFor(ExpectedConditions.visibilityOfElementLocated(By.id(LOADING_PANE_ID)));
        shouldContainText(element(By.id(LOADING_PANE_ID))
                ,"Card added successfully.");
        clickWhenReady(By.id(CONFIRM_BT_ID));
    }
}
