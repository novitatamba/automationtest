package com.nvt.automation.constants;

/**
 * Created by mac Thanh Chuong 10/3/17.
 */
public interface MerchantWSEndPoint {
    String LOGIN = "/merchantappws/1.2/merchantapp/Login";
    String GET_PROMO_CODE = "/merchantappws/1.2/merchantapp/GetPromoCode";
    String SEND_OPEN_BILL ="/merchantappws/1.2/MerchantApp/SendOpenBill";
}
