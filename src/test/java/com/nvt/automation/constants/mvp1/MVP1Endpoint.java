package com.nvt.automation.constants.mvp1;

/**
 * Created by Thanh Chuong on 22/5/17.
 */
public interface MVP1Endpoint {
    String VALIDATION = "v1/consumers/consumer/validation/";
    String LOGIN = "v1/auth/consumer/login";
    String REFRESH = "v1/auth/consumer/refresh";
    String REGISTRATION = "v1/consumers/consumer/registration";
    String REQUEST_OTP = "v1/consumers/consumer/otp";
    String EMAIL_VERIFICATION = "v1/consumers/consumer/self/checkemailverification";
}
