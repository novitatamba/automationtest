/*
 * LiquidPay
 */
package com.nvt.automation.features.web;

import com.nvt.automation.steps.web.GmailSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)

public class LoginGmailTest {

    @Managed
    WebDriver driver;

    @Steps
    GmailSteps gmailSteps;

    public LoginGmailTest(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
    }

    @Test()
    public void loginWithValidCredentialTest() throws Exception{
        gmailSteps.loginWithValidCredential();
        gmailSteps.enterValidCredential("liquidpayauto@gmail.com","Abc@12345");
    }
    
    @AfterClass
    public static void tearDown() {
        
    }


}
