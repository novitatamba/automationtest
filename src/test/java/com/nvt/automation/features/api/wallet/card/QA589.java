package com.nvt.automation.features.api.wallet.card;

import com.nvt.automation.databuilder.WalletCardTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Liquidpay on 4/29/2017.
 */
@RunWith(SerenityRunner.class)

@WithTags(
        {
                @WithTag(type="Regression", name="WalletCard" ),
        }
)
public class QA589 {

    @Steps
    ConsumerSteps consumerWS;
    Consumer consumerMetaData;

    @Before
    public void beforeMethod() throws Exception
    {
        consumerMetaData = WalletCardTestDataBuilder.prepareConsumer();
        consumerWS.cleanMailBox(consumerMetaData);
        consumerWS.login(consumerMetaData);
        consumerWS.deleteCardIfExist(consumerMetaData.getCard());
    }

    @Issue("#QA-589")
    @Title("Consumer Able to add Diners Card")
    @Test
    public void addDinersCard() throws Exception
    {
        consumerWS.updateCardDetails(consumerMetaData.getCard());
        consumerWS.shouldBeAbleToAddCard(consumerMetaData.getCard());
        consumerWS.shouldReceiveNewCardAddedConfirmation(consumerMetaData.getCard());
    }

    @AfterClass
    public static void tearDown()
    {

    }
}
