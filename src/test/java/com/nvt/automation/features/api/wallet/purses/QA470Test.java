/*
 * LiquidPay
 */
package com.nvt.automation.features.api.wallet.purses;

import com.nvt.automation.databuilder.WalletPursesTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Wallet Purses"),
        }
)
public class QA470Test {

    @Steps
    ConsumerSteps consumer;

    //Test data
    Consumer consumerMetaData;

    public QA470Test(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
        consumerMetaData = WalletPursesTestDataBuilder.prepareConsumer();
        consumer.login(consumerMetaData);
        consumer.addPurseIfNotExist(consumerMetaData.getPurse());
    }

    @Issue("#QA-470")
    @Title("Verify when deleting Liquid Cash not yet activate will remain not active when added")
    @Test
    public void deleteLiquidCashNotYetActivateTest() throws Exception{
        consumer.shouldBeAbleToDeletePurse(consumerMetaData.getPurse());
        consumer.deletedPurseShouldBeGone(consumerMetaData.getPurse());
        consumer.shouldBeAbleToAddPurse(consumerMetaData.getPurse());
        consumer.purseStatusShouldBeInActive(consumerMetaData.getPurse());

    }

    @AfterClass
    public static void tearDown() {

    }
}
