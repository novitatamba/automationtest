/*
 * LiquidPay
 */
package com.nvt.automation.features.api.liquidcash;

import com.nvt.automation.databuilder.LCTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Liquid Cash"),
        }
)
public class QA34Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps sender;

    @Steps
    DatabaseHelper databaseHelper;

    //Test data
    Consumer senderUser;
    Consumer recipientUser;

    public QA34Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        senderUser = LCTestDataBuilder.prepareSenderUser();
        recipientUser = LCTestDataBuilder.prepareRecipientUSer();
        recipientUser.setNumber(databaseHelper.getMobileByEmail(recipientUser.getEmail()));

    }

    @Issue("#QA-34")
    @Title("Cash transfer is not allowed if exceed Maximum Transfer per Transaction")
    @Test
    public void cashTransferIsNotAllowedIfExceedMaximumTranferPerTransaction() throws Exception{
        double amount = 101;
        sender.login(senderUser);
        double balanceOfSenderBeforeTransfer = sender.getCurrentBalance(senderUser);
        sender.isNotAllowToTransferWithExceededMaximumAmountPerTransaction(recipientUser,amount);
        double balanceOfSenderAfterTransfer = sender.getCurrentBalance(senderUser);
        sender.verifyThatCurrentBalanceShouldUpdateCorrectly(balanceOfSenderAfterTransfer,
                balanceOfSenderBeforeTransfer);

    }
    
    @AfterClass
    public static void tearDown() throws Exception{
    }


}
