/*
 * LiquidPay
 */
package com.nvt.automation.features.api.liquidcash;

import com.nvt.automation.constants.NotificationFormat;
import com.nvt.automation.databuilder.LCTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Liquid Cash"),
        }
)
public class QA386Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps sender;

    @Steps(uniqueInstance = true)
    ConsumerSteps recipient;

    @Steps
    DatabaseHelper databaseHelper;

    //Test data
    Consumer senderUser;
    Consumer recipientUser;

    public QA386Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        senderUser = LCTestDataBuilder.prepareSenderUser();
        recipientUser = LCTestDataBuilder.prepareRecipientUSer();
        recipientUser.setNumber(databaseHelper.getMobileByEmail(recipientUser.getEmail()));

    }

    @Issue("#QA-386")
    @Title("Cash transfer is allowed for Sender activate with NRIC and Recipient activate with FIN")
    @Test
    public void cashTransferIsAllowedForSenderActiveWithNRICAndRecipientActiveWithFINTest() throws Exception{
        double amount = 1.0;
        sender.login(senderUser);
        recipient.login(recipientUser);
        double balanceOfSenderBeforeTransfer = sender.getCurrentBalance(senderUser);
        double balanceOfRecipientBeforeTransfer = recipient.getCurrentBalance(recipientUser);
        recipient.updateNotificationList();
        sender.transferCashTo(recipientUser,amount);
        double balanceOfSenderAfterTransfer = sender.getCurrentBalance(senderUser);
        sender.verifyThatCurrentBalanceShouldUpdateCorrectly(balanceOfSenderAfterTransfer,
                balanceOfSenderBeforeTransfer-amount);
        double balanceOfRecipientAfterTransfer = recipient.getCurrentBalance(recipientUser);
        recipient.verifyThatCurrentBalanceShouldUpdateCorrectly(balanceOfRecipientAfterTransfer,
                balanceOfRecipientBeforeTransfer+amount);
        //Check notification
        String expectedNotification = String.format(NotificationFormat.TRANSFER_TO_ACTIVATED_CONSUMER,
                sender.getFullName(),amount);
        recipient.verifyThatUserReceiveExpectedNotification(expectedNotification);
    }
    
    @AfterClass
    public static void tearDown() throws Exception{
    }


}
