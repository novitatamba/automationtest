/*
 * LiquidPay
 */
package com.nvt.automation.features.api.liquidcash;

import com.nvt.automation.databuilder.LCTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Liquid Cash"),
        }
)
public class QA32Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps sender;

    @Steps
    DatabaseHelper databaseHelper;

    //Test data
    Consumer senderUser;
    Consumer recipientUser;

    public QA32Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        senderUser = LCTestDataBuilder.prepareSenderUser();
        recipientUser = LCTestDataBuilder.prepareRecipientUSer();
        recipientUser.setNumber(databaseHelper.getMobileByEmail(recipientUser.getEmail()));

    }

    @Issue("#QA-32")
    @Title("Verify customer can't transfer invalid transfer amount")
    @Test
    public void verifyCustomerCannotTransferInvalidAmount() throws Exception{
        double amount = 0.00;
        sender.login(senderUser);
        double balanceOfSenderBeforeTransfer = sender.getCurrentBalance(senderUser);
        sender.isNotAllowedToTransferInvalidAmount(recipientUser,amount);
        double balanceOfSenderAfterTransfer = sender.getCurrentBalance(senderUser);
        sender.verifyThatCurrentBalanceShouldUpdateCorrectly(balanceOfSenderAfterTransfer,
                balanceOfSenderBeforeTransfer);

    }
    
    @AfterClass
    public static void tearDown() throws Exception{
    }


}
