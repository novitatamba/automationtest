/*
 * LiquidPay
 */
package com.nvt.automation.features.api.liquidcash;

import com.nvt.automation.databuilder.LCTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Liquid Cash"),
        }
)
public class QA26Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps sender;

    @Steps
    DatabaseHelper databaseHelper;

    //Test data
    Consumer senderUser;
    Consumer recipientUser;

    public QA26Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        senderUser = LCTestDataBuilder.prepareSenderUser();
        recipientUser = LCTestDataBuilder.prepareRecipientUSer();
        recipientUser.setNumber(databaseHelper.getMobileByEmail(recipientUser.getEmail()));

    }

    @Issue("#QA-26")
    @Title("Cash transfer is not allowed for Sender activate with NRIC and Recipient activate with PP")
    @Test
    public void cashTransferIsNotAllowedForSenderActiveWithNRICAndRecipientActiveWithPPTest() throws Exception{
        double amount = 1.0;
        sender.login(senderUser);
        double balanceOfSenderBeforeTransfer = sender.getCurrentBalance(senderUser);
        sender.isNotAllowedToTransferCashToPPUser(recipientUser,amount);
        double balanceOfSenderAfterTransfer = sender.getCurrentBalance(senderUser);
        sender.verifyThatCurrentBalanceShouldUpdateCorrectly(balanceOfSenderAfterTransfer,
                balanceOfSenderBeforeTransfer);

    }
    
    @AfterClass
    public static void tearDown() throws Exception{
    }


}
