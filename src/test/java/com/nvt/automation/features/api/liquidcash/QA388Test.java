/*
 * LiquidPay
 */
package com.nvt.automation.features.api.liquidcash;

import com.nvt.automation.databuilder.LCTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Liquid Cash"),
        }
)
public class QA388Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps consumer;

    @Steps
    DatabaseHelper databaseHelper;

    //Test data
    Consumer consumerMetaData;

    public QA388Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        consumerMetaData = LCTestDataBuilder.prepareSenderUser();
    }

    @Issue("#QA-388")
    @Title("User unable to activate Liquid cash using registered ID No")
    @Test
    public void userUnableToActiveLiquidCashUsingRegisteredID() throws Exception{
        consumer.login(consumerMetaData);
        consumer.shouldBeUnableToActivateLiquidCashUsingRegisteredID("SG",consumerMetaData.getIdNo());

    }
    
    @AfterClass
    public static void tearDown() throws Exception{
    }


}
