/*
 * LiquidPay
 */
package com.nvt.automation.features.api.payment;

import com.nvt.automation.databuilder.DinersTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.model.Merchant;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.MerchantSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Liquidpay
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Payment")
        }
)
public class QA206Test {

    @Steps
    ConsumerSteps customer;

    @Steps
    MerchantSteps merchantSteps;

    //Test data
    Consumer consumer;
    Merchant merchant;

    public QA206Test(){
    }

    @Before
    public void prepareTestData() throws Exception {
        consumer = DinersTestDataBuilder.prepareConsumer();
        merchant = DinersTestDataBuilder.prepareMerchant();


        customer.login(consumer);
        customer.requestBill("LIQUIDMERCHANT","fusionopolis");
        String s = customer.getRequestBill();
        System.out.print(s);
    }

    @Issue("#QA-584")
    @Title("Enable user to do payment by card by Pay@Table")
    @Test
    public void enableUserToDoPaymentByCard() throws Exception {


        merchantSteps.login(merchant);
        merchantSteps.send_open_bill();
        String abc = merchantSteps.send_open_bill();
        System.out.print(abc);
        customer.login(consumer);
        String getPayCode =  customer.openBill();
        System.out.print(getPayCode);
        String get = customer.getPaymentCode();
        System.out.print(get);
        customer.checkWhetherCardExisting(consumer.getCard());
    }

    @AfterClass
    public static void tearDown() {

    }

}
