/*
 * LiquidPay
 */
package com.nvt.automation.features.api.wallet.card;

import com.nvt.automation.databuilder.WalletCardTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Wallet Card"),
        }
)
public class QA538Test {

    @Steps
    ConsumerSteps consumer;

    //Test data
    Consumer consumerMetaData;

    public QA538Test(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
        consumerMetaData = WalletCardTestDataBuilder.prepareConsumer();
        consumer.cleanMailBox(consumerMetaData);
        consumer.login(consumerMetaData);
        consumer.deleteCardIfExist(consumerMetaData.getCard());
    }

    @Issues({"#QA-538","#QA-149"})
    @Title("System will send confirmation to email when adding new VISA CREDIT card")
    @Test
    public void systemWillSendConfirmationToEmailWhenAddingCard() throws Exception{
        consumer.updateCardDetails(consumerMetaData.getCard());
        consumer.shouldBeAbleToAddCard(consumerMetaData.getCard());
        consumer.shouldReceiveNewCardAddedConfirmation(consumerMetaData.getCard());
    }

    @AfterClass
    public static void tearDown() {

    }


}
