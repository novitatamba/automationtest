/*
 * LiquidPay
 */
package com.nvt.automation.features.api.account_info;

import com.nvt.automation.databuilder.AccountInfoTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Account Info")
        }
)
public class QA226Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps sender;

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    //Test data
    Consumer consumer;

    public QA226Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        consumer = AccountInfoTestDataBuilder.createUser();
        customer.login(consumer);
        customer.getUserDetailInformation();
    }

    @Issue("#QA-226")
    @Title("Enable user to edit profile information test")
    @Test
    public void enableUserToEditProfileInformationTest(){
        customer.login(consumer);
        customer.updateDetails(consumer.getNewFirstName(),consumer.getNewLastName());
        String a = consumer.getNewFirstName()+consumer.getNewLastName();
        System.out.print(a);
    }
    
    @AfterClass
    public static void tearDown() {
        
    }


}
