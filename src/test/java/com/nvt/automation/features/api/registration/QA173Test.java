/*
 * LiquidPay
 */
package com.nvt.automation.features.api.registration;

import com.nvt.automation.databuilder.RegistrationTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.api.EmailHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Registration"),
        }
)
public class QA173Test {

    @Steps
    ConsumerSteps consumer;

    DatabaseHelper databaseHelper = new DatabaseHelper();

    EmailHelper emailHelper = new EmailHelper();

    //Test data
    Consumer consumerMetaData;

    public QA173Test(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
        consumerMetaData = RegistrationTestDataBuilder.prepareConsumer();
        databaseHelper.deleteUserIfExist(consumerMetaData);
        databaseHelper.changeMobileToRandomIfExist(consumerMetaData.getNumber());
        consumer.cleanSMSMailBox();
        consumer.cleanMailBox(consumerMetaData);
    }

    @Issue("#QA-173")
    @Title("Enable user to register into LiquidPay App using Phone Number")
    @Test
    public void enableUserToRegisterIntoLiquidPayAppUsingPhoneNumber() throws Exception{
        consumer.shouldBeAbleToRequestOTP(consumerMetaData);
        String otp = emailHelper.getOTP();
        consumer.verifyThatUserCanRegister(consumerMetaData, otp,"");
        consumer.shouldReceiveVerificationLink(consumerMetaData);
        consumer.verifyEmailAccount();
        consumer.login(consumerMetaData);
        consumer.accountStatusShouldBeActive();
    }

    @AfterClass
    public static void tearDown() {

    }


}
