/*
 * LiquidPay
 */
package com.nvt.automation.features.api.registration;

import com.nvt.automation.databuilder.RegistrationTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.api.EmailHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Registration"),
        }
)
public class QA179Test {

    @Steps
    ConsumerSteps consumer;

    DatabaseHelper databaseHelper = new DatabaseHelper();

    EmailHelper emailHelper = new EmailHelper();

    //Test data
    Consumer consumerMetaData;

    public QA179Test(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
        consumerMetaData = RegistrationTestDataBuilder.prepareConsumer();
    }

    @Issue("#QA-179")
    @Title("Disallow user to register with empty email")
    @Test
    public void disallowUserToRegisterWithEmptyEmail() throws Exception{
        consumer.verifyThatUserCanNotRegisterWithEmptyEmail(consumerMetaData,"");
    }

    @AfterClass
    public static void tearDown() {

    }


}
