/*
 * LiquidPay
 */
package com.nvt.automation.features.api.registration;

import com.nvt.automation.databuilder.RegistrationTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.api.EmailHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Registration"),
        }
)
public class QA45Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    @Steps
    DatabaseHelper databaseHelper;

    @Steps
    EmailHelper emailHelper;

    //Test data
    Consumer consumer;


    public QA45Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        consumer = RegistrationTestDataBuilder.prepareConsumer();
        databaseHelper.deleteUserIfExist(consumer);
        databaseHelper.changeMobileToRandomIfExist(consumer.getNumber());
    }

    @Issue("#QA-45")
    @Title("Prompt warning if expired promo code has been provided")
    @Test
    public void promptWarningIfExpiredPromoCodeHasBeenProvided() throws Exception{
        String expiredCode = "SKQ4AB";
        customer.shouldBeUnableToRegisterWithInvalidPromoCode(consumer, expiredCode);
    }

    @Issue("#QA-45")
    @Title("Prompt warning if unregistered promo code has been provided")
    @Test
    public void promptWarningIfUnregisteredPromoCodeHasBeenProvided() throws Exception{
        String unregisteredCode = "NYUI8J";
        customer.shouldBeUnableToRegisterWithInvalidPromoCode(consumer, unregisteredCode);
    }
    
    
    @AfterClass
    public static void tearDown() {
        
    }


}
