package com.nvt.automation.features.api.payment;

import com.nvt.automation.databuilder.DinersTestDataBuilder;
import com.nvt.automation.model.Card;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Liquidpay on 5/22/2017.
 */

@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression",name = "Topup")
        }
)
public class QA275Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;
    //Test Data
    Consumer consumer;
    Card card;

    @Before
    public void prepareTestData() throws Exception{
        consumer = DinersTestDataBuilder.prepareConsumer();
        customer.login(consumer);
    }

    @Issue("#QA-275")
    @Title("Top-up SVC Using Liquid Cash")
    @Test

    public void topUpSVCUsingLiquidCash()
    {
        customer.topUpSVCUsingLiquidCash();
        double balanceCustomer = customer.getCurrentBalance(consumer);
        System.out.print("Your Current Balance : "+balanceCustomer);
    }

    @AfterClass
    public static void tearDown(){

    }
}
