/*
 * LiquidPay
 */
package com.nvt.automation.features.api.liquidcash;

import com.nvt.automation.databuilder.LCTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Liquid Cash"),
        }
)
public class QA30Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps sender;

    @Steps
    DatabaseHelper databaseHelper;

    //Test data
    Consumer senderUser;
    Consumer recipientUSer;

    public QA30Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        senderUser = LCTestDataBuilder.prepareSenderUser();
        recipientUSer = LCTestDataBuilder.prepareRecipientUSer();
    }

    @Issue("#QA-30")
    @Title("Verify customer disable transfer into invalid mobile number")
    @Test
    public void verifyCustomerDisableTransferIntoInvalidMobileNumber() throws Exception{
        double amount = 1.0;
        sender.login(senderUser);
        double balanceOfSenderBeforeTransfer = sender.getCurrentBalance(senderUser);
        sender.isNotAllowToTransferToInvalidMobileNumber(recipientUSer,amount);
        double balanceOfSenderAfterTransfer = sender.getCurrentBalance(senderUser);
        sender.verifyThatCurrentBalanceShouldUpdateCorrectly(balanceOfSenderAfterTransfer,
                balanceOfSenderBeforeTransfer);

    }
    
    @AfterClass
    public static void tearDown() throws Exception{
    }


}
