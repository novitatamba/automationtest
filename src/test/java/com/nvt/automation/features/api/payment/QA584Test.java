/*
 * LiquidPay
 */
package com.nvt.automation.features.api.payment;

import com.nvt.automation.databuilder.DinersTestDataBuilder;
import com.nvt.automation.model.Card;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Liquidpay
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Payment")
        }
)
public class QA584Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    //Test data
    Consumer consumer;
    Card card;

    public QA584Test(){
    }

    @Before
    public void prepareTestData() throws Exception {
        consumer = DinersTestDataBuilder.prepareConsumer();
        double amount = 15.00;
        customer.login(consumer);
        customer.requestPayment("LIQUIDMERCHANT","fusionopolis",amount);
        String s = customer.getPaymentCode();
        System.out.print(s);
    }

    @Issue("#QA-584")
    @Title("Enable user to do payment by card")
    @Test
    public void enableUserToDoPaymentByCard(){
        customer.login(consumer);
        customer.checkWhetherCardExisting(consumer.getCard());
    }

    @AfterClass
    public static void tearDown() {

    }


}
