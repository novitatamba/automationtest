/*
 * LiquidPay
 */
package com.nvt.automation.features.api.account_info;

import com.nvt.automation.databuilder.AccountInfoTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Account Info"),
        }
)
public class QA227Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    //Test data
    Consumer consumer;

    public QA227Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        consumer = AccountInfoTestDataBuilder.createUser();

    }

    @Issue("#QA-227")
    @Title("Enable user to change password test")
    @Test
    public void enableUserToChangePasswordTest(){
        customer.login(consumer);
        customer.changePasswordTo(consumer.getPassword(), consumer.getNewPassword());
        customer.logOut();
        customer.verifyThatCustomerCanLoginWithNewPassword(consumer, consumer.getNewPassword());
        customer.changePasswordTo(consumer.getNewPassword(), consumer.getPassword());
        customer.logOut();
        customer.verifyThatCustomerCanLoginWithNewPassword(consumer, consumer.getPassword());
    }
    
    
    
    @AfterClass
    public static void tearDown() {
        
    }


}
