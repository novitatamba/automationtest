/*
 * LiquidPay
 */
package com.nvt.automation.features.api.registration;

import com.nvt.automation.databuilder.RegistrationTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.api.EmailHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Registration"),
        }
)
public class QA107Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    @Steps
    DatabaseHelper databaseHelper;

    @Steps
    EmailHelper emailHelper;

    //Test data
    Consumer consumer;

    public QA107Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        consumer = RegistrationTestDataBuilder.prepareConsumer();
        databaseHelper.deleteUserIfExist(consumer);
        databaseHelper.changeMobileToRandomIfExist(consumer.getNumber());
        emailHelper.cleanSMSMailBox();
        emailHelper.cleanMailBox(consumer);
    }

    @Issue("#QA-107")
    @Title("User who does not activate their email will not be allowed to login")
    @Test
    public void userWhoDoesNotActiveEmailWillNotBeAllowedToLogin() throws Exception{
        customer.shouldBeAbleToRequestOTP(consumer);
        String otp = emailHelper.getOTP();
        customer.register(consumer, otp,"");
        customer.login(consumer);
        customer.verifyThatAccountStatusIsPending();
    }
    
    
    
    @AfterClass
    public static void tearDown() {

    }


}
