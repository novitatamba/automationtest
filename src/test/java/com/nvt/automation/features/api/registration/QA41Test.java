/*
 * LiquidPay
 */
package com.nvt.automation.features.api.registration;

import com.nvt.automation.databuilder.RegistrationTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.model.Merchant;
import com.nvt.automation.model.PromoCode;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.api.EmailHelper;
import com.nvt.automation.steps.api.MerchantSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Registration"),
        }
)
public class QA41Test {

    @Steps
    ConsumerSteps consumer;

    @Steps
    MerchantSteps merchant;

    DatabaseHelper databaseHelper = new DatabaseHelper();

    EmailHelper emailHelper = new EmailHelper();

    //Test data
    Consumer consumerMetaData;
    Merchant merchantMetaData;

    public QA41Test(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
        consumerMetaData = RegistrationTestDataBuilder.prepareConsumer();
        merchantMetaData = RegistrationTestDataBuilder.prepareMerchant();
        databaseHelper.deleteUserIfExist(consumerMetaData);
        databaseHelper.changeMobileToRandomIfExist(consumerMetaData.getNumber());
        consumer.cleanSMSMailBox();
        consumer.cleanMailBox(consumerMetaData);
    }

    @Issue("#QA-41")
    @Title("Promo code entry is available for new sign up user")
    @Test
    public void promoCodeEntryIsAvailableForNewSignUpUSer() throws Exception{
        merchant.login(merchantMetaData);
        PromoCode prePromoCode = merchant.getPromoCode();
        consumer.shouldBeAbleToRequestOTP(consumerMetaData);
        String otp = emailHelper.getOTP();
        consumer.verifyThatUserCanRegister(consumerMetaData, otp,prePromoCode.getCode());
        consumer.shouldReceiveVerificationLink(consumerMetaData);
        consumer.verifyEmailAccount();
        consumer.login(consumerMetaData);
        consumer.accountStatusShouldBeActive();
        PromoCode currPromoCode = merchant.getPromoCode();
        merchant.shouldUpdatePromoCodeCorrectly(prePromoCode,currPromoCode, 1);
        consumer.shouldUpdateCurrentBalanceTo(consumerMetaData, currPromoCode.getFactor());
    }

    @AfterClass
    public static void tearDown() {

    }


}
