/*
 * LiquidPay
 */
package com.nvt.automation.features.api.wallet.card;

import com.nvt.automation.databuilder.WalletCardTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Wallet Card"),
        }
)
public class QA154Test {

    @Steps
    ConsumerSteps consumerWS;

    //Test data
    Consumer consumerMetaData;

    public QA154Test(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
        consumerMetaData = WalletCardTestDataBuilder.prepareConsumer();
        consumerWS.cleanMailBox(consumerMetaData);
        consumerWS.login(consumerMetaData);
        consumerWS.deleteCardIfExist(consumerMetaData.getCard());
    }

    @Issue("#QA-154")
    @Title("System will send confirmation to email when adding new MASTER CREDIT card")
    @Test
    public void systemWillSendConfirmationToEmailWhenAddingCard() throws Exception{
        consumerWS.updateCardDetails(consumerMetaData.getCard());
        consumerWS.shouldBeAbleToAddCard(consumerMetaData.getCard());
        consumerWS.shouldReceiveNewCardAddedConfirmation(consumerMetaData.getCard());
    }

    @AfterClass
    public static void tearDown() {

    }




}
