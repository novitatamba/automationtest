/*
 * LiquidPay
 */
package com.nvt.automation.features.api.registration;

import com.nvt.automation.databuilder.RegistrationTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.api.EmailHelper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Registration"),
                @WithTag(type = "Regression", name = "Known Issues")
        }
)
public class LDT295Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    @Steps
    DatabaseHelper databaseHelper;

    @Steps
    EmailHelper emailHelper;

    //Test data
    Consumer consumer;

    public LDT295Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        consumer = RegistrationTestDataBuilder.prepareConsumer();
        databaseHelper.deleteUserIfExist(consumer);
        databaseHelper.changeMobileToRandomIfExist(consumer.getNumber());
        emailHelper.cleanSMSMailBox();
        emailHelper.cleanMailBox(consumer);
    }

    @Issue("#LDT-295")
    @Title("Prevent user to continue registration process as key in invalid PASSWORD")
    @Test
    public void preventUserToContinueRegistrationProcessAsKeyInvalidPassword() throws Exception{
        customer.shouldBeAbleToRequestOTP(consumer);
        String otp = emailHelper.getOTP();
        customer.verifyThatUserCannotRegisterWithInvalidPassword(consumer, otp, "", "12345");
    }

    @AfterClass
    public static void tearDown() {
        
    }
}