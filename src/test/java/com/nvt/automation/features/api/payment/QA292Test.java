package com.nvt.automation.features.api.payment;

/**
 * Created by Liquidpay on 5/22/2017.
 */

import com.nvt.automation.databuilder.DinersTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Payment")
        }
)

public class QA292Test {
    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    //Test Data
    Consumer consumer;

    public QA292Test(){

    }

    @Before
    public void prepareTestData() throws Exception{
        consumer = DinersTestDataBuilder.prepareConsumer();
        customer.login(consumer);
        double balanceCustomer = customer.getCurrentBalance(consumer);
        System.out.print("Your Current Balance : "+balanceCustomer);
        double amount = 15.00;
        customer.requestPayment("LIQUIDMERCHANT","fusionopolis",amount);
    }

    @Issue("#QA-292")
    @Title("Unable Customer to pay with insufficient balance")
    @Test

    public void unableCustomerPayWithInsufficientBalance(){
        customer.login(consumer);
        customer.notAllowedToPayWithInsufficientBalance();
    }
}
