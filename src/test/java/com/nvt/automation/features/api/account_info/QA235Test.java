/*
 * LiquidPay
 */
package com.nvt.automation.features.api.account_info;

import com.nvt.automation.databuilder.AccountInfoTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Regression", name = "Account Info"),
        }
)
public class QA235Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    //Test data
    Consumer consumer;

    public QA235Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        consumer = AccountInfoTestDataBuilder.createUser();

    }

    @Issue("#QA-235")
    @Title("Enable user to logout from application test")
    @Test
    public void enableUserToLogoutFromApplicationTest(){
        customer.login(consumer);
        customer.logOut();
        customer.verifyThatSystemShouldPreventUserUseObsoleteSessionKey();
    }
    
    @AfterClass
    public static void tearDown() {
        
    }


}
