package com.nvt.automation.features.api.payment;

import com.nvt.automation.databuilder.DinersTestDataBuilder;
import com.nvt.automation.model.Card;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Liquidpay on 5/23/2017.
 */

@RunWith(SerenityRunner.class)

@WithTags(
        {
                @WithTag(type = "Regression", name = "TopUp")
        }
)

public class QA281Test {

    @Steps(uniqueInstance = true)
    ConsumerSteps customer;

    //Test Data
    Consumer consumer;
    Card card;

    @Before
    public void prepareTestData() throws Exception{
        consumer = DinersTestDataBuilder.prepareConsumer();
        customer.login(consumer);
    }

    @Issue("#QA-281")
    @Title("Top Up SVC using Card")
    @Test

    public void topUpSVCUsingCard()
    {
        customer.shouldBeAbleTopUpByCard(consumer.getCard());
    }
}


