/*
 * LiquidPay
 */
package com.nvt.automation.features.mobile.liquidcash;

import com.nvt.automation.databuilder.LCTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.mobile.ConsumerMobileSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Mobile", name = "Liquid Cash"),
        }
)
public class QA274Test {


    @Managed
    WebDriver driver;

    @Steps
    ConsumerMobileSteps consumer;

    //Test data
    Consumer consumerMetadata;

    public QA274Test(){
    }
    
    @Before
    public void prepareTestData() throws Exception {
        consumerMetadata = LCTestDataBuilder.prepareSenderUser();

    }

    @Issue("#QA-274")
    @Title("User enable to Top Up Liquid Cash using ENETS")
    @Test
    public void userEnableToTopUpLiquidCashUsingENETS() throws Exception{
        long amount = 10;
        consumer.shouldAbleToLogin(consumerMetadata);
        consumer.selectLiquidCashPurse();
        consumer.topUpUsingENETS(amount);


    }
    
    @AfterClass
    public static void tearDown() throws Exception{
    }



}
