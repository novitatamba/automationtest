/*
 * LiquidPay
 */
package com.nvt.automation.features.mobile.diners;

import com.nvt.automation.databuilder.DinersTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.mobile.ConsumerMobileSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Mobile", name = "Diners")
        }
)
public class QA582Test {

    @Managed
    WebDriver appiumDriver;

    @Steps
    ConsumerMobileSteps consumer;

    @Steps
    ConsumerSteps consumerWS;

    Consumer consumerMetaData;

    public QA582Test(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
        consumerMetaData = DinersTestDataBuilder.prepareConsumer();
        consumerWS.login(consumerMetaData);
        consumerWS.deleteCardIfExist(consumerMetaData.getCard());
    }

    @Issue("#QA-582")
    @Test()
    @Title("Consumer allowed to add Diner Virtual Account manually in the purse after successfully add Diner Credit Card")
    public void consumerAllowedToAddDinerVCTest(){
        consumer.shouldAbleToLogin(consumerMetaData);
        consumer.shouldBeAbleToAddNewDinersCard(consumerMetaData.getCard());
    }
    
    @AfterClass
    public static void tearDown() {
        
    }


}
