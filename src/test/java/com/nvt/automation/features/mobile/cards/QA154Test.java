/*
 * LiquidPay
 */
package com.nvt.automation.features.mobile.cards;

import com.nvt.automation.databuilder.WalletCardTestDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.ConsumerSteps;
import com.nvt.automation.steps.mobile.ConsumerMobileSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "Mobile", name = "Cards")
        }
)
public class QA154Test {

    @Managed
    WebDriver appiumDriver;

    @Steps
    ConsumerMobileSteps consumer;

    @Steps
    ConsumerSteps consumerWS;

    Consumer consumerMetaData;

    public QA154Test(){
    }
    
    @Before
    public void beforeMethod() throws Exception {
        consumerMetaData = WalletCardTestDataBuilder.prepareConsumer();
        consumerWS.cleanMailBox(consumerMetaData);
        consumerWS.login(consumerMetaData);
        consumerWS.deleteCardIfExist(consumerMetaData.getCard());
    }

    @Issue("#QA-154")
    @Test()
    @Title("System will send confirmation to email when adding new MASTER CREDIT card")
    public void consumerAllowedToAddDinerVCTest() throws Exception{
        consumer.shouldAbleToLogin(consumerMetaData);
        consumer.shouldBeAbleToAddMasterCard(consumerMetaData.getCard());
        consumerWS.shouldReceiveNewCardAddedConfirmation(consumerMetaData.getCard());

    }
    
    @AfterClass
    public static void tearDown() {
        
    }


}
