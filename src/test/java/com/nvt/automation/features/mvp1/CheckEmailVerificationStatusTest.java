package com.nvt.automation.features.mvp1;

import com.nvt.automation.steps.mvp1.MVP1ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Anita on 5/29/2017.
 */

@WithTags(
        {
                @WithTag(type = "mvp1", name = "EmailVerification"),
        }
)
@RunWith(SerenityRunner.class)

public class CheckEmailVerificationStatusTest {

    @Steps
    MVP1ConsumerSteps consumerSteps;

    @Test
    public void checkEmailVerificationStatusWithVerifiedEmail(){
        consumerSteps.loginWithValidUserAndPassword();
        String access_token = consumerSteps.getAccessToken();
        System.out.print("Access Token = "+access_token);
        consumerSteps.checkEmailVerificationStatus();
    }

    @Test
    public void checkEmailVerificationStatusWithUnverifiedEmail(){
        consumerSteps.loginWithValidCredentialsWithPendingStatus();
        String access_token = consumerSteps.getAccessToken();
        System.out.print("Access Token = "+access_token);
        consumerSteps.checkEmailVerificationStatus();
    }

    @Test
    public void checkEmailVerificationWithInvalidEmailAddress(){
        consumerSteps.loginWithInvalidEmailAddress();
        consumerSteps.failedCredentialsToCheckEmailVerificationStatus();
    }

    @Test
    public void checkEmailVerificationWithInvalidPassword(){
        consumerSteps.loginWithInvalidPassword();
        consumerSteps.failedCredentialsToCheckEmailVerificationStatus();
    }

    @Test
    public void checkEmailVerificationWithUnregisteredEmailAddress(){
        consumerSteps.loginWithUnregisteredEmailAddress();
        consumerSteps.failedCredentialsToCheckEmailVerificationStatus();
    }

    @Test
    public void checkEmailVerificationWithEmptyEmailAddress(){
        consumerSteps.loginWithEmptyEmailAddress();
        consumerSteps.failedCredentialsToCheckEmailVerificationStatus();
    }

    @Test
    public void checkEmailVerificationWithEmptyPassword(){
        consumerSteps.loginWithEmptyPassword();
        consumerSteps.failedCredentialsToCheckEmailVerificationStatus();
    }

    @Test
    public void checkEmailVerificationWithEmptyEmailAddressAndEmptyPassword(){
        consumerSteps.loginWithEmptyEmailAddressAndEmptyPassword();
        consumerSteps.failedCredentialsToCheckEmailVerificationStatus();
    }

}
