/*
 * LiquidPay
 */
package com.nvt.automation.features.mvp1;

import com.nvt.automation.steps.mvp1.MVP1ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Thanh Chuong
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "mvp1", name = "Validation"),
        }
)
public class ValidationTest {


    @Steps
    MVP1ConsumerSteps mvp1ConsumerSteps;

    public ValidationTest(){
    }
    
    @Before
    public void beforeMethod() throws Exception {

    }

    @Test
    public void enterValidValueForEmailAndPromoCodeTest() throws Exception{
        mvp1ConsumerSteps.enterValidEmailAndPromoCode();
    }

    @Test
    public void enterRegisteredEmailTest() throws Exception{
        mvp1ConsumerSteps.enterEmailThatHasBeenRegistered();
    }

    @Test
    public void enterInvalidEmailFormatTest() throws Exception{
        mvp1ConsumerSteps.enterInvalidEmailFormat();
    }

    @Test
    public void enterEmptyEmailTest() throws Exception{
        mvp1ConsumerSteps.enterEmptyEmail();
    }

    @Test
    public void enterInvalidPasswordTest() throws Exception{
        mvp1ConsumerSteps.enterInvalidPasswordTest();
    }

    @Test
    public void enterEmptyPasswordTest() throws Exception{
        mvp1ConsumerSteps.enterEmptyPasswordTest();
    }

    @Test
    public void enterRegisteredMobileNumber() throws Exception{
        mvp1ConsumerSteps.enterRegisteredMobileNumber();
    }
    @Test
    public void enterValidMobileNumberTest() throws Exception{
        mvp1ConsumerSteps.enterValidMobileNumberTest();
    }

    @Test
    public void enterValidDialingCodeTest() throws Exception{
        mvp1ConsumerSteps.enterValidDialingCodeTest();
    }

    @Test
    public void enterInvalidDialingCodeTest() throws Exception{
        mvp1ConsumerSteps.enterInvalidDialingCodeTest();
    }

    @Test
    public void enterEmptyDialingCodeTest() throws Exception{
        mvp1ConsumerSteps.enterEmptyDialingCodeTest();
    }

    @AfterClass
    public static void tearDown() {

    }


}
