package com.nvt.automation.features.mvp1;
import com.nvt.automation.databuilder.mvp1.RegisterDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.mvp1.MVP1ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by cantikmanis on 30-May-17.
 */
@WithTags(
        {
                @WithTag(type = "mvp1", name = "RegisterWithOTPVerifications"),
        }
)
@RunWith(SerenityRunner.class)

public class RegistrationWithOTPVerificationTest {
    @Steps
    MVP1ConsumerSteps consumerSteps;

    @Steps
    DatabaseHelper databaseHelper;

    //Test data
    Consumer consumer;

    @Before
    public void prepareTestData() throws Exception {
        consumer = RegisterDataBuilder.registerDataWithValidCredential();
        databaseHelper.deleteUserIfExist(consumer);
        databaseHelper.changeMobileToRandomIfExist(consumer.getNumber());
        databaseHelper.deleteNumberIfExist(consumer.getNumber());
    }

   @Test
    public void registrationWithValidOTPVerification()throws Exception{
        //just called this send OTP, however, it would harcode in registration as 000000
        consumerSteps.sendOTPRequestToValidMobileNumber();
        consumerSteps.registrationWithValidOTPVerification(consumer);
    }

    @Test
    public void registrationWithEmptyOTPVerification()
    {
        consumerSteps.sendOTPRequestToValidMobileNumber();
        consumerSteps.registrationWithEmptyOTPVerification(consumer);
    }

    @Test
    public void registrationWithInvalidOTPVerification()
    {
        consumerSteps.sendOTPRequestToValidMobileNumber();
        consumerSteps.registrationWithInvalidOTPVerification(consumer);
    }

    @Test
    public void registrationWithExpiredOTPVerification()throws Exception
    {
        consumerSteps.sendOTPRequestToValidMobileNumber();
        databaseHelper.updateCustomerOTP(consumer.getNumber());
        consumerSteps.registrationWithExpiredOTPVerification(consumer);

    }

   @Test
    public void registrationWithInvalidFormatOTPVerification()
    {
        consumerSteps.sendOTPRequestToValidMobileNumber();
        consumerSteps.registrationWithInvalidFormatOTPVerification(consumer);
    }

    @Test
    public void registrationWithRegisteredEmailAddress()
    {
        consumerSteps.sendOTPRequestToValidMobileNumber();
        consumerSteps.registrationWithRegisteredEmailAddress(consumer);
    }


    @Test
    public void registrationWithInvalidEmailAddress()
    {
        consumerSteps.sendOTPRequestToValidMobileNumber();
        consumerSteps.registrationWithInvalidEmailAddress(consumer);
    }
}
