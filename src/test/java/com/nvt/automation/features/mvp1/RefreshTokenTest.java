package com.nvt.automation.features.mvp1;
import com.nvt.automation.databuilder.mvp1.LoginDataBuilder;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.mvp1.MVP1ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Anita on 5/30/2017.
 */

@WithTags(
        {
                @WithTag(type = "mvp1", name = "RefreshToken"),
        }
)
@RunWith(SerenityRunner.class)

public class RefreshTokenTest {

    @Steps
    MVP1ConsumerSteps consumerSteps;

    @Steps
    DatabaseHelper databaseHelper;

    //Test data
    Consumer consumer;

    @Before
    public void prepareTestData() throws Exception {
        consumer = LoginDataBuilder.buildConsumerWithValidCredentialPendingStatus();
    }

    @Test
    public void refreshTokenAfterLoginToVerifyEmailAddressStatusActive(){
        consumerSteps.loginWithValidUserAndPassword();
        consumerSteps.getRefreshNewToken();
        consumerSteps.checkEmailVerificationStatusWithNewAccessToken();
    }

    @Test
    public void refreshTokenAfterLoginToVerifyEmailAddressStatusPending(){
        consumerSteps.loginWithValidCredentialsWithPendingStatus();
        consumerSteps.getRefreshNewToken();
        consumerSteps.checkEmailVerificationStatusWithNewAccessToken();
    }

    @Test
    public void verifyUserCannotVerifyEmailWithInvalidAccessToken(){
        consumerSteps.loginWithValidUserAndPassword();
        consumerSteps.getRefreshNewToken();
        consumerSteps.checkEmailVerificationStatusWithOldAccessToken();
    }

    @Test
    public void verifyRefreshTokenExpiryAfter15MinutesLogin() throws Exception{
        consumerSteps.loginWithValidCredentialsWithPendingStatus();
        String a = consumer.getEmail();
        System.out.print(a);
        String s = "get "+databaseHelper.getExpiredDate(consumer.getEmail());
        System.out.print(s);
        databaseHelper.updateExpiryDate(consumer.getEmail());
        consumerSteps.expiryRefreshToken();
    }

    @Test
    public void refreshTokenAfterRegistrationToVerifyEmailAddress(){
        consumerSteps.sendOTPRequestToValidMobileNumber();
        consumerSteps.registerWithValidCredential();
        consumerSteps.getRefreshNewToken();
        consumerSteps.checkEmailVerificationStatusWithNewAccessToken();
    }

    @AfterClass
    public static void teardown(){

    }
}