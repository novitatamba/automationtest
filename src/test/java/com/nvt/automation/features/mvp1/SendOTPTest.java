package com.nvt.automation.features.mvp1;

import com.nvt.automation.databuilder.mvp1.SendOTPRequestDataBuilder;
import com.nvt.automation.steps.api.DatabaseHelper;
import com.nvt.automation.steps.mvp1.MVP1ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Novitasari Tamba on 29/5/17.
 */
@RunWith(SerenityRunner.class)
@WithTags(
        {
                @WithTag(type = "mvp1", name = "SendOTP"),
        }
)
public class SendOTPTest {

    @Steps
    MVP1ConsumerSteps consumerSteps;


    @Steps
    DatabaseHelper databaseHelper;

    @Test
    public void sendOTPRequestToValidMobileNumber()throws Exception{
        String MobileNumber = SendOTPRequestDataBuilder.validNumbertoRequestOTP();
        databaseHelper.deleteNumberIfExist(MobileNumber);
        consumerSteps.sendOTPRequestToValidMobileNumber();
    }

    @Test
    public void sendOTPRequestToRegisteredMobileNumber(){
        consumerSteps.sendOTPRequestToRegisterdMobileNumber();
    }

    @Test
    public void sendOTPRequestToInvalidMobileNumber(){
        consumerSteps.sendOTPRequestToInvalidMobileNumber();
    }

    @Test
    public void sendOTPRequestToEmptyDiallingCode(){
        consumerSteps.sendOTPRequestToEmptyDiallingCode();
    }

    @Test
    public void sendOTPRequestToInvalidDiallingCode(){
        consumerSteps.sendOTPRequestToInvalidDiallingCode();
    }

    @Test
    public void sendOTPRequestToEmptyMobileNumber(){
        consumerSteps.sendOTPRequestToEmptyMobileNumber();
    }





}
