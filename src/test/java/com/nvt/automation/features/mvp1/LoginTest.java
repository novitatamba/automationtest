package com.nvt.automation.features.mvp1;

import com.nvt.automation.steps.mvp1.MVP1ConsumerSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Thanh Chuong on 23/5/17.
 */
@WithTags(
        {
                @WithTag(type = "mvp1", name = "Validation"),
        }
)
@RunWith(SerenityRunner.class)
public class LoginTest {



    @Steps
    MVP1ConsumerSteps consumerSteps;

    @Test
    public void loginWithValidUserAndPasswordTest(){
        consumerSteps.loginWithValidUserAndPassword();
    }

    @Test
    public void loginWithInvalidEmailAddress(){
        consumerSteps.loginWithInvalidEmailAddress();
    }

    @Test
    public void loginWithInvalidPassword(){
        consumerSteps.loginWithInvalidPassword();
    }

    @Test
    public void loginWithUnregisteredEmailAddress(){
        consumerSteps.loginWithUnregisteredEmailAddress();
    }

    @Test
    public void loginWithEmptyEmailAddress(){
        consumerSteps.loginWithEmptyEmailAddress();
    }

    @Test
    public void loginWithEmptyPassword() {
        consumerSteps.loginWithEmptyPassword();
    }

    @Test
    public void loginWithEmptyEmailAddressAndEmptyPassword(){
        consumerSteps.loginWithEmptyEmailAddressAndEmptyPassword();
    }

    @Test
    public void loginWithValidCredentialsWithPendingStatus(){
        consumerSteps.loginWithValidCredentialsWithPendingStatus();
    }

}
