package com.nvt.automation.databuilder.mvp1;

import org.json.JSONObject;

/**
 * Created by Novitasari Tamba on 29/5/17.
 */
public class SendOTPRequestDataBuilder {

    private static String DIALING_CODE = "dialing_code";
    private static String MOBILE_NUMBER = "mobile_number";

    public static String validNumbertoRequestOTP(){
        String number ="84019158";
        return number;
    }

    public static String buildOTPRequestJsonWithValidNuber(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"65")
                .put(MOBILE_NUMBER,"82314000");
        return json.toString();
    }


    public static String buildOTPRequestJsonWithInvalidNumber(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"65")
                .put(MOBILE_NUMBER,"87771231762");
        return json.toString();
    }

    public static String buildOTPRequestJsonWithEmptyDiallingCode(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"")
                .put(MOBILE_NUMBER,"87776762");
        return json.toString();
    }

    public static String buildOTPRequestJsonWithInvalidDiallingCode(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"6285")
                .put(MOBILE_NUMBER,"81325815");
        return json.toString();
    }


    public static String buildOTPRequestJsonWithRegisteredMobileNumber(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"65")
                .put(MOBILE_NUMBER,"90278898");
        return json.toString();
    }

    public static String buildOTPRequestJsonWithEmptyMobileNumber(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"65")
                .put(MOBILE_NUMBER,"");
        return json.toString();
    }
}
