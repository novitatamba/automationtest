package com.nvt.automation.databuilder.mvp1;

import com.nvt.automation.model.Consumer;
import org.json.JSONObject;

/**
 * Created by Thanh Chuong on 23/5/17.
 */
public class LoginDataBuilder {
    private static String USERNAME = "username";
    private static String PASSWORD = "password";
    private static String DEVICE_TYPE = "device_type";
    private static String DEVICE_ID = "device_id";
    private static String DEVICE_UID = "device_uid";
    private static String DEVICE_MODEL = "device_model";
    private static String DEVICE_OS = "device_os";

    public static String buildLoginJson(Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(USERNAME,consumer.getEmail())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_TYPE,"I")
                .put(DEVICE_ID,"0df738df25c57c8a1d39bd8112e332375b4a402aa4cfcb3ea3e8198ade2173e9")
                .put(DEVICE_UID,"87DC6089-EDDA-491F-8C05-FF0FDFA9EDB8")
                .put(DEVICE_MODEL,"iPhone7,2")
                .put(DEVICE_OS,"10.1.1");
        return json.toString();
    }

    public static Consumer buildConsumerWithValidCredential(){
        Consumer consumer = new Consumer()
                .setEmail("hutagaola@gmail.com")
                .setPassword("pass1234");
        return consumer;
    }

    public static Consumer buildConsumerWithInvalidEmailAddress(){
        Consumer consumer = new Consumer()
                .setEmail("hutagaola")
                .setPassword("pass1234");
        return consumer;

    }

    public static Consumer buildConsumerWithInvalidPassword(){
        Consumer consumer = new Consumer()
                .setEmail("hutagaola@gmail.com")
                .setPassword("Pass12345");
        return consumer;
    }

    public static Consumer buildConsumerWithUnregisteredEmailAddress(){
        Consumer consumer = new Consumer()
                .setEmail("unregistered@gmail.com")
                .setPassword("pass1234");
        return consumer;
    }

    public static Consumer buildConsumerWithEmptyEmailAddress(){
        Consumer consumer = new Consumer()
                .setEmail("")
                .setPassword("pass1234");
        return consumer;
    }

    public static Consumer buildConsumerWithEmptyPassword(){
        Consumer consumer = new Consumer()
                .setEmail("hutagaola@gmail.com")
                .setPassword("");
        return consumer;
    }

    public static Consumer buildConsumerWithEmptyEmailAddressAndEmptyPassword(){
        Consumer consumer = new Consumer()
                .setEmail("")
                .setPassword("");
        return consumer;
    }

    public static Consumer buildConsumerWithValidCredentialPendingStatus(){
        Consumer consumer = new Consumer()
                .setEmail("Anit@mailinator.com")
                .setPassword("pass1234");
        return consumer;
    }

}
