package com.nvt.automation.databuilder.mvp1;

import org.json.JSONObject;

/**
 * Created by Thanh Chuong on 22/5/17.
 */
public class ValidationDataBuilder {

    private static String EMAIL = "email_address";
    private static String PROMO_CODE = "promo_code";
    private static String PASSWORD = "password";
    private static String MOBILE_NUMBER = "mobile_number";
    private static String DIALING_CODE = "dialing_code";

    public static String buildValidEmailAndPromoCode(){
        JSONObject json = new JSONObject();
        json.put(EMAIL,"chuongtuthanh@mailinator.com")
                .put(PROMO_CODE,"NE9GE9");
//                .put(PROMO_CODE,"NE9GE0");
        return json.toString();
    }

    public static String buildEmailHasBeenRegistered(){
        JSONObject json = new JSONObject();
        json.put(EMAIL,"chuongtuthanh@gmail.com");
        return json.toString();
    }

    public static String buildInvalidEmailFormat(){
        JSONObject json = new JSONObject();
        json.put(EMAIL,"chuongtuthanh@gmail");
        return json.toString();
    }

    public static String buildEmptyEmail(){
        JSONObject json = new JSONObject();
        json.put(EMAIL,"");
        return json.toString();
    }

    public static String buildInvalidPassword(){
        JSONObject json = new JSONObject();
        json.put(PASSWORD,"pass");
        return json.toString();
    }

    public static String buildEmptyPassword(){
        JSONObject json = new JSONObject();
        json.put(PASSWORD,"");
        return json.toString();
    }

    public static String buildRegisteredMobile(){
        JSONObject json = new JSONObject();
        json.put(MOBILE_NUMBER,"86269117");
        return json.toString();
    }
    

    public static String buildValidMobileNumber(){
        JSONObject json = new JSONObject();
        json.put(MOBILE_NUMBER,"81325815");
        return json.toString();
    }

    public static String buildValidDialingCode(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"65");
        return json.toString();
    }

    public static String buildInvalidDialingCode(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"1894");
        return json.toString();
    }

    public static String buildEmptyDialingCode(){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"");
        return json.toString();
    }
}