package com.nvt.automation.databuilder.mvp1;

import com.nvt.automation.model.Consumer;
import org.json.JSONObject;

/**
 * Created by Anita on 5/29/2017.
 */
public class RegisterDataBuilder {

    private static String EMAIL_ADDRESS = "email_address";
    private static String DIALING_CODE = "dialing_code";
    private static String MOBILE_NUMBER = "mobile_number";
    private static String OTP = "otp";
    private static String FIRST_NAME = "first_name";
    private static String LAST_NAME ="last_name";
    private static String PASSWORD = "password";
    private static String DEVICE_ID = "device_id";
    private static String DEVICE_UID = "device_uid";
    private static String DEVICE_MODEL = "device_model";
    private static String DEVICE_OS = "device_os";
    private static String DEVICE_TYPE = "device_type";
    private static String REGISTERED_SOURCE = "registered_source";
    private static String PROMO_CODE = "promo_code";

    public static String buildRegisterJson (Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(EMAIL_ADDRESS,consumer.getEmail())
                .put(DIALING_CODE,"65")
                .put(MOBILE_NUMBER,consumer.getNumber())
                .put(OTP,"000000")
                .put(FIRST_NAME,consumer.getFirstName())
                .put(LAST_NAME,consumer.getLastName())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_ID,"0df738df25c57c8a1d39bd8112e332375b4a402aa4cfcb3ea3e8198ade2173e9")
                .put(DEVICE_UID,"87DC6089-EDDA-491F-8C05-FF0FDFA9EDB8")
                .put(DEVICE_MODEL,"iPhone7,2")
                .put(DEVICE_OS,"10.1.1")
                .put(DEVICE_TYPE,"I")
                .put(REGISTERED_SOURCE,"")
                .put(PROMO_CODE,"");
        return json.toString();
    }

    public static String buildRequestOTPJson(Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(DIALING_CODE,"65")
                .put(MOBILE_NUMBER,consumer.getNumber());
        return json.toString();
    }

    public static Consumer buildRegisterDataWithValidCredential(){
        Consumer consumer = new Consumer()
                .setEmail("anita19@mailinator.com")
                .setNumber("82314000")
                .setFirstName("Anita")
                .setLastName("Automation")
                .setPassword("pass1234");
        return consumer;
    }

    public static Consumer registerDataWithValidCredential(){
        Consumer consumer = new Consumer()
                .setEmail("autouser@mailinator.com")
                .setDialingCode("65")
                .setNumber("81325815")
                .setFirstName("Auto")
                .setLastName("user")
                .setPassword("pass1234");
        return consumer;
    }

    public static String resgistrationWithValidOTPVerification (Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(EMAIL_ADDRESS,consumer.getEmail())
                .put(DIALING_CODE,consumer.getDialingCode())
                .put(MOBILE_NUMBER,consumer.getNumber())
                .put(OTP,"000000")
                .put(FIRST_NAME,consumer.getFirstName())
                .put(LAST_NAME,consumer.getLastName())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_ID,consumer.getDeviceId())
                .put(DEVICE_UID,consumer.getDeviceUID())
                .put(DEVICE_MODEL,consumer.getDeviceModel())
                .put(DEVICE_OS,consumer.getDeviceOS())
                .put(DEVICE_TYPE,"I")
                .put(REGISTERED_SOURCE,"")
                .put(PROMO_CODE,"");
        return json.toString();
    }

    public static String resgistrationWithEmptyOTPVerification (Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(EMAIL_ADDRESS,consumer.getEmail())
                .put(DIALING_CODE,consumer.getDialingCode())
                .put(MOBILE_NUMBER,consumer.getNumber())
                .put(OTP,"")
                .put(FIRST_NAME,consumer.getFirstName())
                .put(LAST_NAME,consumer.getLastName())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_ID,consumer.getDeviceId())
                .put(DEVICE_UID,consumer.getDeviceUID())
                .put(DEVICE_MODEL,consumer.getDeviceModel())
                .put(DEVICE_OS,consumer.getDeviceOS())
                .put(DEVICE_TYPE,"I")
                .put(REGISTERED_SOURCE,"")
                .put(PROMO_CODE,"");
        return json.toString();
    }

    public static String resgistrationWithInvalidOTPVerification (Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(EMAIL_ADDRESS,consumer.getEmail())
                .put(DIALING_CODE,consumer.getDialingCode())
                .put(MOBILE_NUMBER,consumer.getNumber())
                .put(OTP,"586932")
                .put(FIRST_NAME,consumer.getFirstName())
                .put(LAST_NAME,consumer.getLastName())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_ID,consumer.getDeviceId())
                .put(DEVICE_UID,consumer.getDeviceUID())
                .put(DEVICE_MODEL,consumer.getDeviceModel())
                .put(DEVICE_OS,consumer.getDeviceOS())
                .put(DEVICE_TYPE,"I")
                .put(REGISTERED_SOURCE,"")
                .put(PROMO_CODE,"");
        return json.toString();
    }

    public static String registrationWithInvalidFormatOTPVerification (Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(EMAIL_ADDRESS,consumer.getEmail())
                .put(DIALING_CODE,consumer.getDialingCode())
                .put(MOBILE_NUMBER,consumer.getNumber())
                .put(OTP,"aaabbb")
                .put(FIRST_NAME,consumer.getFirstName())
                .put(LAST_NAME,consumer.getLastName())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_ID,consumer.getDeviceId())
                .put(DEVICE_UID,consumer.getDeviceUID())
                .put(DEVICE_MODEL,consumer.getDeviceModel())
                .put(DEVICE_OS,consumer.getDeviceOS())
                .put(DEVICE_TYPE,"I")
                .put(REGISTERED_SOURCE,"")
                .put(PROMO_CODE,"");
        return json.toString();
    }

    public static String registrationWithExpiredOTPVerification (Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(EMAIL_ADDRESS,consumer.getEmail())
                .put(DIALING_CODE,consumer.getDialingCode())
                .put(MOBILE_NUMBER,consumer.getNumber())
                .put(OTP,"000000")
                .put(FIRST_NAME,consumer.getFirstName())
                .put(LAST_NAME,consumer.getLastName())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_ID,consumer.getDeviceId())
                .put(DEVICE_UID,consumer.getDeviceUID())
                .put(DEVICE_MODEL,consumer.getDeviceModel())
                .put(DEVICE_OS,consumer.getDeviceOS())
                .put(DEVICE_TYPE,"I")
                .put(REGISTERED_SOURCE,"")
                .put(PROMO_CODE,"");
        return json.toString();
    }

    public static String registrationWithRegisteredEmailAddress (Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(EMAIL_ADDRESS,"novitasari.tamba@liquidpay.com")
                .put(DIALING_CODE,consumer.getDialingCode())
                .put(MOBILE_NUMBER,consumer.getNumber())
                .put(OTP,"000000")
                .put(FIRST_NAME,consumer.getFirstName())
                .put(LAST_NAME,consumer.getLastName())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_ID,consumer.getDeviceId())
                .put(DEVICE_UID,consumer.getDeviceUID())
                .put(DEVICE_MODEL,consumer.getDeviceModel())
                .put(DEVICE_OS,consumer.getDeviceOS())
                .put(DEVICE_TYPE,"I")
                .put(REGISTERED_SOURCE,"")
                .put(PROMO_CODE,"");
        return json.toString();
    }

    public static String registrationWithInvalidEmailAddress (Consumer consumer){
        JSONObject json = new JSONObject();
        json.put(EMAIL_ADDRESS,"novitasari.tamba")
                .put(DIALING_CODE,consumer.getDialingCode())
                .put(MOBILE_NUMBER,consumer.getNumber())
                .put(OTP,"000000")
                .put(FIRST_NAME,consumer.getFirstName())
                .put(LAST_NAME,consumer.getLastName())
                .put(PASSWORD,consumer.getPassword())
                .put(DEVICE_ID,consumer.getDeviceId())
                .put(DEVICE_UID,consumer.getDeviceUID())
                .put(DEVICE_MODEL,consumer.getDeviceModel())
                .put(DEVICE_OS,consumer.getDeviceOS())
                .put(DEVICE_TYPE,"I")
                .put(REGISTERED_SOURCE,"")
                .put(PROMO_CODE,"");
        return json.toString();
    }












}
