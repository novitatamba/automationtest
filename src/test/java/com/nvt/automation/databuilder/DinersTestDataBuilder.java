package com.nvt.automation.databuilder;

import com.nvt.automation.model.Card;
import com.nvt.automation.model.Consumer;
import com.nvt.automation.model.Merchant;
import com.nvt.automation.web.util.SerenityUtil;
import com.nvt.automation.web.util.XlsDataSrc;

/**
 * Created by Thanh Chuong on 20/2/17.
 */
public class DinersTestDataBuilder extends TestDataBuilder{

    private static final String DATA_FILE_PATH = SerenityUtil.getEnv("data.diners");
    private static final String EMAIL = "B";
    private static final String PWD = "C";
    private static final String CARD_NO = "D";
    private static final String CARD_CVV = "E";
    private static final String CARD_NAME = "F";
    private static final String CARD_EXPIRY = "G";
    private static final String CARD_DEFAULT = "H";
    private static final String GMAIL_PWD = "I";
    private static final String MERCHANT_ID = "J";
    private static final String OUTLET_ID = "K";
    private static final String Amt ="L";
    private static final String MERCHANT_USER = "M";
    private static final String MERCHANT_PWD = "N";

    public static Consumer prepareConsumer() throws Exception{
        Consumer user = new Consumer();
        if(obj==null || !DATA_FILE_PATH.equalsIgnoreCase(currentDataFile)){
            prepareTestData(SerenityUtil.getIssue(),DATA_FILE_PATH);
        }
        for(Object[] row: obj){
            String[] array = (String[]) row[1];
            if(array[0].equalsIgnoreCase(SerenityUtil.getIssue())){
                user.setEmail(array[XlsDataSrc.toNumber(EMAIL)].trim())
                        .setPassword(array[XlsDataSrc.toNumber(PWD)].trim())
                        .setGmailPassword(array[XlsDataSrc.toNumber(GMAIL_PWD)]);
                Card card = new Card();
                card.setCardNo(array[XlsDataSrc.toNumber(CARD_NO)].trim())
                        .setCvv(array[XlsDataSrc.toNumber(CARD_CVV)].trim())
                        .setName(array[XlsDataSrc.toNumber(CARD_NAME)].trim())
                        .setExpiry(array[XlsDataSrc.toNumber(CARD_EXPIRY)].trim())
                        .setDefaultCard(array[XlsDataSrc.toNumber(CARD_DEFAULT)].trim());
                user.addCard(card);
            }
        }
        return user;
    }

    public static Merchant prepareMerchant() throws Exception{
        Merchant user = new Merchant();
        if(obj==null || !DATA_FILE_PATH.equalsIgnoreCase(currentDataFile)){
            prepareTestData(SerenityUtil.getIssue(),DATA_FILE_PATH);
        }
        for(Object[] row: obj){
            String[] array = (String[]) row[1];
            if(array[0].equalsIgnoreCase(SerenityUtil.getIssue())){
                user.setUserName(array[XlsDataSrc.toNumber(MERCHANT_USER)].trim())
                        .setPassword(array[XlsDataSrc.toNumber(MERCHANT_PWD)].trim());
            }
        }
        return user;
    }


}
